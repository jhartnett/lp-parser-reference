#pragma once
#ifndef GRAMMAR_INDEX_INCLUDED
#error "Cannot include Firsts.h directly. Use Index.h"
#endif
#include "../Index.h"

namespace grammar::meta {
    Firsts getFirsts(const Grammar& grammar);
    template<class Iterable=std::initializer_list<Symbol>>
    Set<Symbol> getFirsts(const Firsts& firsts, const Iterable& alpha); // NOLINT(readability-redundant-declaration)
    template<class Iterable=std::initializer_list<Symbol>, class Iterable2=std::initializer_list<Symbol>>
    Set<Symbol> getFirsts(const Firsts& firsts, const Iterable& alpha, const Iterable2& lookaheads); // NOLINT(readability-redundant-declaration)
    template<class Iterable=std::initializer_list<Symbol>>
    bool insertFirsts(const Firsts& firsts, const Iterable& alpha, Set<Symbol>& stringFirsts); // NOLINT(readability-redundant-declaration)
    template<class Iterable=std::initializer_list<Symbol>, class Iterable2=std::initializer_list<Symbol>>
    bool insertFirsts(const Firsts& firsts, const Iterable& alpha, const Iterable2& lookaheads, Set<Symbol>& stringFirsts); // NOLINT(readability-redundant-declaration)
}