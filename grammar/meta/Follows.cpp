#include "../Index.h"

namespace grammar::meta {
    Map<Symbol, Set<Symbol>> getFollows(const Grammar& grammar, const Map<Symbol, Set<Symbol>>& firsts){
        Map<Symbol, Set<Symbol>> follows { };
        for(auto& A : grammar.alphabet())
            follows.try_emplace(A);
        follows.at(Symbol::START).insert(Symbol::END);
        bool change;
        do{
            change = false;
            for(auto& [A, alpha] : grammar.rules()){
                for(size_t i = 0; i < alpha.size(); i++){
                    const Symbol& B = alpha.at(i);
                    auto gamma = std::make_pair(alpha.begin() + i + 1, alpha.end());
                    change |= insertFirsts(firsts, gamma, follows.at(A), follows.at(B));
                }
            }
        }while(change);
        return follows;
    }
}