#pragma once
#ifndef GRAMMAR_INDEX_INCLUDED
#error "Cannot include Firsts.cpp.h directly. Use Index.h"
#endif
#include "../Index.h"

namespace grammar::meta {
    template<class Iterable>
    Set<Symbol> getFirsts(const Firsts& firsts, const Iterable& alpha){
        Set<Symbol> stringFirsts { };
        insertFirsts(firsts, alpha, stringFirsts);
        return stringFirsts;
    }

    template<class Iterable, class Iterable2>
    Set<Symbol> getFirsts(const Firsts& firsts, const Iterable& alpha, const Iterable2& lookaheads){
        Set<Symbol> stringFirsts { };
        insertFirsts(firsts, alpha, lookaheads, stringFirsts);
        return stringFirsts;
    }

    template<class Iterable>
    bool insertFirsts(const Firsts& firsts, const Iterable& alpha, Set<Symbol>& stringFirsts){
        return insertFirsts(firsts, alpha, {Symbol::EPSILON}, stringFirsts);
    }

    template<class Iterable, class Iterable2>
    bool insertFirsts(const Firsts& firsts, const Iterable& alpha, const Iterable2& lookaheads, Set<Symbol>& stringFirsts){
        bool change = false;
        for(const Symbol& A : alpha){
            bool fallthrough = false;
            for(const Symbol& B : firsts.at(A)){
                if(B == Symbol::EPSILON)
                    fallthrough = true;
                else
                    change |= stringFirsts.insert(B).second;
            }
            if(!fallthrough)
                return change;
        }
        for(const Symbol& A : lookaheads)
            change |= stringFirsts.insert(A).second;
        return change;
    }
}