#pragma once
#ifndef GRAMMAR_INDEX_INCLUDED
#error "Cannot include Follows.h directly. Use Index.h"
#endif
#include "../Index.h"

namespace grammar::meta {
    Follows getFollows(const Grammar& grammar, const Map<Symbol, Set<Symbol>>& firsts);
}