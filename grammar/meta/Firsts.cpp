#include "../Index.h"

namespace grammar::meta {
    Firsts getFirsts(const Grammar& grammar) {
        Firsts firsts { };
        for(auto& A : grammar.alphabet())
            firsts[A].insert(A);
        bool change;
        do{
            change = false;
            for(auto& [A, alpha] : grammar.rules())
                change |= insertFirsts(firsts, alpha, firsts.at(A));
        }while(change);
        return firsts;
    }
}