#pragma once
#ifndef GRAMMAR_INDEX_INCLUDED
#error "Cannot include Item.cpp.h directly. Use Index.h"
#endif
#include "Index.h"
#include "Item.h"


namespace grammar {
    template<size_t LA>
    Item<LA>::Item(Rule rule, size_t i, Array<Symbol, LA> lookahead) :
        rule(std::move(rule)),
        i(i),
        lookahead(std::move(lookahead))
    { }

    template<size_t LA>
    template<size_t LA2, typename>
    Item<LA>::Item(const Item<LA2>& item) :
        Item(item.rule, item.i)
    { }

    template<size_t LA>
    template<size_t LA2, typename>
    Item<LA>::Item(Item<LA2>&& item) :
        Item(std::move(item.rule), item.i)
    { }

    template<size_t LA>
    template<size_t LA2, typename>
    auto Item<LA>::operator =(const Item<LA2>& item) -> Item& {
        rule = item.rule;
        i = item.i;
        return *this;
    }

    template<size_t LA>
    template<size_t LA2, typename>
    auto Item<LA>::operator =(Item<LA2>&& item) -> Item& {
        rule = std::move(item.rule);
        i = item.i;
        return *this;
    }

    template<size_t LA>
    const Symbol& Item<LA>::prev() const {
        return i > 0 ? rule[i - 1] : Symbol::EPSILON;
    }

    template<size_t LA>
    const Symbol& Item<LA>::next() const {
        return i < rule.right().size() ? rule[i] : Symbol::EPSILON;
    }

    template<size_t LA>
    Item<LA>& Item<LA>::operator ++() {
        return (*this) += 1;
    }

    template<size_t LA>
    const Item<LA> Item<LA>::operator ++(int) & {
        Item item(*this);
        (*this) += 1;
        return item;
    }

    template<size_t LA>
    Item<LA>& Item<LA>::operator +=(size_t j) {
        i += j;
        return *this;
    }

    template<size_t LA>
    Item<LA> Item<LA>::operator +(size_t j) const {
        Item item(*this);
        item += j;
        return item;
    }

    template<size_t LA>
    Item<LA>& Item<LA>::operator --() {
        return (*this) -= 1;
    }

    template<size_t LA>
    const Item<LA> Item<LA>::operator --(int) & {
        Item item(*this);
        (*this) -= 1;
        return item;
    }

    template<size_t LA>
    Item<LA>& Item<LA>::operator -=(size_t j) {
        i -= j;
        return *this;
    }

    template<size_t LA>
    Item<LA> Item<LA>::operator -(size_t j) const {
        Item item(*this);
        item -= j;
        return item;
    }

    template<size_t LA>
    bool operator ==(const Item<LA>& a, const Item<LA>& b) {
        if(a.rule != b.rule || a.i != b.i)
            return false;
        for(size_t i = 0; i < LA; i++){
            if(a.lookahead[i] != b.lookahead[i])
                return false;
        }
        return true;
    }

    template<size_t LA>
    bool operator !=(const Item<LA>& a, const Item<LA>& b) {
        return !(a == b);
    }

    template<size_t LA>
    std::ostream& operator <<(std::ostream& os, const Item<LA>& item) {
        os << item.rule.left();
        os << "=";
        size_t i = 0;
        for(auto& sym : item.rule.right()){
            if(i == item.i)
                os << "•";
            else if(i != 0)
                os << " ";
            os << sym;
            i++;
        }
        if(i == item.i)
            os << "•";
        if constexpr(LA > 0){
            os << " [";
            for(size_t j = 0; j < LA; j++){
                if(j != 0)
                    os << " ";
                os << item.lookahead[j];
            }
            os << "]";
        }
        return os;
    }
}

template<size_t LA>
size_t std::hash<grammar::Item<LA>>::operator ()(const grammar::Item<LA>& item) const {
    size_t hash = util::get_hash(item.rule, item.i);
    for(size_t i = 0; i < LA; i++)
        util::collect_hash(hash, item.lookahead[i]);
    return hash;
}