#pragma once
#ifndef GRAMMAR_INDEX_INCLUDED
#error "Cannot include Item.h directly. Use Index.h"
#endif
#include "Index.h"

namespace grammar {
    template<size_t LA>
    bool operator ==(const Item<LA>& a, const Item<LA>& b); // NOLINT(readability-redundant-declaration)
    template<size_t LA>
    bool operator !=(const Item<LA>& a, const Item<LA>& b); // NOLINT(readability-redundant-declaration)
    template<size_t LA>
    std::ostream& operator <<(std::ostream& os, const Item<LA>& item); // NOLINT(readability-redundant-declaration)

    template<size_t LA=0>
    class Item {
    public:
        using iterator = Rule::iterator;
        using const_iterator = Rule::const_iterator;

        Rule rule;
        size_t i;
        Array<Symbol, LA> lookahead;

        constexpr Item() noexcept : rule(), i(), lookahead() {
            for(size_t i = 0; i < LA; i++)
                lookahead[i] = nullptr;
        }
        Item(Rule rule, size_t i=0, Array<Symbol, LA> lookahead=Array<Symbol, LA>()); // NOLINT(google-explicit-constructor)
        template<size_t LA2, typename=std::enable_if_t<LA2 != LA>>
        Item(const Item<LA2>& item); // NOLINT(google-explicit-constructor)
        template<size_t LA2, typename=std::enable_if_t<LA2 != LA>>
        Item(Item<LA2>&& item); // NOLINT(google-explicit-constructor)

        template<size_t LA2, typename=std::enable_if_t<LA2 != LA>>
        Item& operator =(const Item<LA2>& item);
        template<size_t LA2, typename=std::enable_if_t<LA2 != LA>>
        Item& operator =(Item<LA2>&& item);

        const Symbol& prev() const;
        const Symbol& next() const;

        Item& operator ++();
        const Item operator ++(int) &;
        const Item operator ++(int) && = delete;
        Item& operator +=(size_t j);
        Item operator +(size_t j) const;

        Item& operator --();
        const Item operator --(int) &;
        const Item operator --(int) && = delete;
        Item& operator -=(size_t j);
        Item operator -(size_t j) const;

        template<size_t N>
        decltype(auto) get() {
            if constexpr(N == 0) return rule.left();
            else if constexpr(N == 1) return std::make_pair(rule.begin(), rule.begin() + i);
            else if constexpr(N == 2) return next();
            else if constexpr(N == 3) return std::make_pair(rule.begin() + std::min(i + 1, rule.size()), rule.end());
            else if constexpr(N == 4) return lookahead;
        }
        template<size_t N>
        decltype(auto) get() const {
            if constexpr(N == 0) return rule.left();
            else if constexpr(N == 1) return std::make_pair(rule.begin(), rule.begin() + i);
            else if constexpr(N == 2) return next();
            else if constexpr(N == 3) return std::make_pair(rule.begin() + std::min(i + 1, rule.size()), rule.end());
            else if constexpr(N == 4) return lookahead;
        }

        friend std::hash<Item>;
        friend bool operator == <LA>(const Item& a, const Item& b);
        friend bool operator != <LA>(const Item& a, const Item& b);
        friend std::ostream& operator << <LA>(std::ostream& os, const Item& item);
    };
}

template<size_t LA>
struct std::hash<grammar::Item<LA>> {
    size_t operator ()(const grammar::Item<LA>& item) const;
};

template<size_t N>
struct std::tuple_size<grammar::Item<N>> : std::integral_constant<size_t, 5> { };
template<>
struct std::tuple_size<grammar::Item<0>> : std::integral_constant<size_t, 4> { };
template<size_t N>
struct std::tuple_element<0, grammar::Item<N>> { using type = grammar::Symbol; };
template<size_t N>
struct std::tuple_element<0, const grammar::Item<N>> { using type = const grammar::Symbol; };
template<size_t N>
struct std::tuple_element<1, grammar::Item<N>> { using type = std::pair<grammar::Rule::iterator, grammar::Rule::iterator>; };
template<size_t N>
struct std::tuple_element<1, const grammar::Item<N>> { using type = std::pair<grammar::Rule::const_iterator, grammar::Rule::const_iterator>; };
template<size_t N>
struct std::tuple_element<2, grammar::Item<N>> { using type = grammar::Symbol; };
template<size_t N>
struct std::tuple_element<2, const grammar::Item<N>> { using type = const grammar::Symbol; };
template<size_t N>
struct std::tuple_element<3, grammar::Item<N>> { using type = std::pair<grammar::Rule::iterator, grammar::Rule::iterator>; };
template<size_t N>
struct std::tuple_element<3, const grammar::Item<N>> { using type = std::pair<grammar::Rule::const_iterator, grammar::Rule::const_iterator>; };
template<size_t N>
struct std::tuple_element<4, grammar::Item<N>> { using type = grammar::Array<grammar::Symbol, N>; };
template<size_t N>
struct std::tuple_element<4, const grammar::Item<N>> { using type = const grammar::Array<grammar::Symbol, N>; };

