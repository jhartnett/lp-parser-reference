#include "Index.h"
#include "Grammar.h"


namespace grammar {
    namespace detail {
        GrammarBody::GrammarBody(Set<Rule> rules) :
            alphabet(),
            rules(),
            ruleSets()
        {
            for(auto& rule : rules)
                add(rule);
        }

        void GrammarBody::add(grammar::Rule rule) {
            if(rules.insert(rule).second){
                auto& [left, right] = rule;
                alphabet.insert(left);
                alphabet.insert(right.begin(), right.end());
                ruleSets[rule.left()].insert(std::move(rule));
            }
        }

        void GrammarBody::clear() {
            alphabet.clear();
            rules.clear();
            ruleSets.clear();
        }
    }

    Set<Symbol>& Grammar::malphabet() {
        return UNCONST(alphabet);
    }

    const Set<Symbol>& Grammar::alphabet() const {
        return ref().alphabet;
    }

    Set<Rule>& Grammar::mrules() {
        return UNCONST(rules);
    }

    Set<Rule>& Grammar::mrules(const Symbol& left) {
        return ref().ruleSets[left];
    }

    const Set<Rule>& Grammar::rules() const {
        return ref().rules;
    }

    const Set<Rule>& Grammar::rules(const Symbol& left) const {
        auto it = ref().ruleSets.find(left);
        if(it == ref().ruleSets.end()){
            static Set<Rule> empty { };
            return empty;
        }
        return it->second;
    }

    size_t Grammar::size() const {
        return rules().size();
    }

    void Grammar::add(Rule rule) {
        ref().add(std::move(rule));
    }

    void Grammar::clear() {
        ref().clear();
    }

    Grammar::Grammar() :
        Grammar(Set<Rule>())
    { }

    Grammar::Grammar(Set<Rule> rules) :
        Object(util::body_tag, std::move(rules))
    { }
}