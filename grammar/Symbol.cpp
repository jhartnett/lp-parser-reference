#include "Index.h"
#include "Symbol.h"

#include <iomanip>
#include <variant>


namespace grammar {
    namespace detail {
        namespace {
            std::string escape(const std::string& str){
                std::ostringstream os { };
                for(auto& c : str){
                    if(c == '"' || c == '\\' || (0 <= c && c <= 0x1F))
                        os << "\\u" << std::hex << std::setw(4) << std::setfill('0') << (int)c;
                    else
                        os << c;
                }
                return os.str();
            }
        }

        SymbolBody::SymbolBody(std::string name, bool isStatic) :
            name(std::move(name)),
            isStatic(isStatic)
        { }

        std::ostream& operator <<(std::ostream& os, const SymbolBody& symbol) {
            os << escape(symbol.name);
            return os;
        }
    }

    const Symbol Symbol::EPSILON { "ε", true }; // NOLINT(cert-err58-cpp)
    const Symbol Symbol::START { "START", true }; // NOLINT(cert-err58-cpp)
    const Symbol Symbol::END { "END", true }; // NOLINT(cert-err58-cpp)
    const Symbol Symbol::chars[256];

    namespace detail {
        void initChars(){
            for(size_t i = 0; i < 256; i++)
                const_cast<Symbol&>(Symbol::chars[i]) = Symbol(std::string(1, (char)i), true);
        }
        [[maybe_unused]] static nullptr_t static_init_helper = (initChars(), nullptr); // NOLINT(cert-err58-cpp)
    }

    const Symbol& Symbol::getChar(char c) {
        return chars[static_cast<unsigned char>(c)];
    }

    std::string& Symbol::name() {
        return UNCONST(name);
    }

    const std::string& Symbol::name() const {
        return ref().name;
    }

    bool Symbol::isStatic() const {
        return ref().isStatic;
    }

    Symbol::Symbol(char c) noexcept :
        Object(getChar(c))
    { }

    Symbol::Symbol(std::string name) noexcept :
        Symbol(std::move(name), false)
    { }

    Symbol::Symbol(std::string name, bool isStatic) noexcept :
        Object(util::body_tag, std::move(name), isStatic)
    { }
}