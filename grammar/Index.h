#pragma once

#include <string>
#include <memory>
#include <cassert>
#include <iostream>
#include "../util/Index.h"

#define GRAMMAR_INDEX_INCLUDED

namespace grammar {
    using namespace util::types;

    struct Symbol;
    struct Rule;
    template<size_t LA>
    struct Item;
    class Grammar;

    using Firsts = Map<Symbol, Set<Symbol>>;
    using Follows = Map<Symbol, Set<Symbol>>;
}

#include "Symbol.h"
#include "Rule.h"
#include "Item.h"
#include "Grammar.h"
#include "meta/Firsts.h"
#include "meta/Follows.h"

#include "Item.cpp.h"
#include "meta/Firsts.cpp.h"

#undef GRAMMAR_INDEX_INCLUDED

namespace grammar {
    using grammar::meta::getFirsts;
    using grammar::meta::getFollows;
}