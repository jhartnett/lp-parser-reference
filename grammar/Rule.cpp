#include "Index.h"

namespace grammar {
    namespace detail {
        RuleBody::RuleBody(Symbol left, List<Symbol> right) :
            left(std::move(left)),
            right(std::move(right))
        { }

        std::ostream& operator <<(std::ostream& os, const RuleBody& rule) {
            os << rule.left << "=";
            size_t i = 0;
            for(auto& sym : rule.right){
                if(i != 0)
                    os << " ";
                os << sym;
                i++;
            }
            return os;
        }
    }

    Symbol& Rule::left() {
        return UNCONST(left);
    }

    const Symbol& Rule::left() const {
        return ref().left;
    }

    List<Symbol>& Rule::right() {
        return UNCONST(right);
    }

    const List<Symbol>& Rule::right() const {
        return ref().right;
    }

    bool Rule::empty() const {
        return right().empty();
    }

    size_t Rule::size() const {
        return right().size();
    }

    Symbol& Rule::operator [](size_t i) {
        return right()[i];
    }

    const Symbol& Rule::operator [](size_t i) const {
        return right()[i];
    }

    auto Rule::begin() -> iterator {
        return right().begin();
    }

    auto Rule::begin() const -> const_iterator {
        return cbegin();
    }

    auto Rule::cbegin() const -> const_iterator {
        return right().begin();
    }

    auto Rule::end() -> iterator {
        return right().end();
    }

    auto Rule::end() const -> const_iterator {
        return cend();
    }

    auto Rule::cend() const -> const_iterator {
        return right().end();
    }

    Rule::Rule(Symbol left, List<Symbol> right) :
        Object(util::body_tag, std::move(left), std::move(right))
    { }
}