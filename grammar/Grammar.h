#pragma once
#ifndef GRAMMAR_INDEX_INCLUDED
#error "Cannot include Grammar.h directly. Use Index.h"
#endif
#include "Index.h"

namespace grammar {
    namespace detail {
        struct GrammarBody {
            Set<Symbol> alphabet;
            Set<Rule> rules;
            Map<Symbol, Set<Rule>> ruleSets;

            explicit GrammarBody(Set<Rule> rules);

            void add(Rule rule);
            void clear();
        };
    }

    class Grammar : public util::Object<detail::GrammarBody>{
    private:
        Set<Symbol>& malphabet();
    public:
        const Set<Symbol>& alphabet() const;
    private:
        Set<Rule>& mrules();
        Set<Rule>& mrules(const Symbol& left);
    public:
        const Set<Rule>& rules() const;
        const Set<Rule>& rules(const Symbol& left) const;

        size_t size() const;
        void add(Rule rule);
        void clear();

        Grammar();
        explicit Grammar(Set<Rule> rules);
    };
}