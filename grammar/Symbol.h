#pragma once
#ifndef GRAMMAR_INDEX_INCLUDED
#error "Cannot include Symbol.h directly. Use Index.h"
#endif
#include "Index.h"

namespace grammar {
    namespace detail {
        struct SymbolBody {
            std::string name;
            bool isStatic;

            explicit SymbolBody(std::string name, bool isStatic);

            friend std::ostream& operator <<(std::ostream& os, const SymbolBody& symbol);
        };

        void initChars();
    }
}

namespace grammar {
    struct Symbol : public util::Object<detail::SymbolBody> {
    public:
        static constexpr uint64_t STATIC_COUNT = 3 + 256;
        static const Symbol EPSILON;
        static const Symbol START;
        static const Symbol END;
    private:
        static const Symbol chars[256];
        friend void detail::initChars();
    public:
        static const Symbol& getChar(char c);

        std::string& name();
        const std::string& name() const;

        bool isStatic() const;

        constexpr Symbol(nullptr_t=nullptr) noexcept : Object() { } // NOLINT(google-explicit-constructor)
        Symbol(char c) noexcept; // NOLINT(google-explicit-constructor)
        Symbol(std::string name) noexcept; // NOLINT(google-explicit-constructor)
    private:
        Symbol(std::string name, bool isStatic) noexcept;
    };
}

template<>
struct std::hash<grammar::Symbol> : grammar::Symbol::IdentityHash { };