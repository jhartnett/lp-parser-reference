#pragma once
#ifndef GRAMMAR_INDEX_INCLUDED
#error "Cannot include Rule.h directly. Use Index.h"
#endif
#include "Index.h"

namespace grammar {
    namespace detail {
        struct RuleBody {
            Symbol left;
            List<Symbol> right;

            RuleBody(Symbol left, List<Symbol> right);

            friend std::ostream& operator <<(std::ostream& os, const RuleBody& rule);
        };
    }

    struct Rule : public util::Object<detail::RuleBody> {
        using iterator = List<Symbol>::iterator;
        using const_iterator = List<Symbol>::const_iterator;

        Symbol& left();
        const Symbol& left() const;

        List<Symbol>& right();
        const List<Symbol>& right() const;

        bool empty() const;
        size_t size() const;
        Symbol& operator [](size_t i);
        const Symbol& operator [](size_t i) const;

        iterator begin();
        const_iterator begin() const;
        const_iterator cbegin() const;

        iterator end();
        const_iterator end() const;
        const_iterator cend() const;

        template<size_t N>
        decltype(auto) get() {
            if constexpr(N == 0) return left();
            else if constexpr(N == 1) return right();
        }
        template<size_t N>
        decltype(auto) get() const {
            if constexpr(N == 0) return left();
            else if constexpr(N == 1) return right();
        }

        constexpr Rule(nullptr_t=nullptr) noexcept : Object() { } // NOLINT(google-explicit-constructor)
        Rule(Symbol left, List<Symbol> right);
    };
}

template<>
struct std::hash<grammar::Rule> : grammar::Rule::IdentityHash {};

template<>
struct std::tuple_size<grammar::Rule> : std::integral_constant<size_t, 2> { };
template<>
struct std::tuple_element<0, grammar::Rule> { using type = grammar::Symbol; };
template<>
struct std::tuple_element<0, const grammar::Rule> { using type = const grammar::Symbol; };
template<>
struct std::tuple_element<1, grammar::Rule> { using type = grammar::List<grammar::Symbol>; };
template<>
struct std::tuple_element<1, const grammar::Rule> { using type = const grammar::List<grammar::Symbol>; };