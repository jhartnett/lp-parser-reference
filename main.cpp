#include <iostream>
#include <fstream>
#include <sstream>
#include "grammar/Index.h"
#include "parser/Index.h"
#include "clike/Index.h"

using namespace grammar;
using namespace parser;
using namespace std::string_literals;

std::string readFile(const std::string& file){
    std::ifstream is(file);
    std::stringstream buffer { };
    buffer << is.rdbuf();
    return buffer.str();
}

int main(int argc, char** argv) {
    if(argc < 1){
        std::cout << "Usage: lp_ref_impl file" << std::endl;
        std::cout << "Parses the given file in the clike grammar" << std::endl;
        return 0;
    }
    std::string file = argv[1];
    std::cout << "Parsing file: " << file << std::endl;
    std::string input = readFile(file);

    auto [grammar, disambiguator] = clike::getGrammar();

    Firsts firsts = getFirsts(grammar);

    Recorder recorder { };

    recorder.clear();
    {
        std::cout << "Building GLR" << std::endl;
        GLRTable table = GLR::build(grammar, firsts, disambiguator);
        std::cout << "Testing GLR" << std::endl;
        bool success = GLR::parse(table, input, recorder);
        if(!success){
            std::cout << "The given file is not valid clike" << std::endl;
            return 1;
        }
        std::cout << recorder << std::endl;
    }
    recorder.clear();
    {
        std::cout << "Building LP(LL1, GLR)" << std::endl;
        GLRTable table = GLR::build(grammar, firsts, disambiguator, false, true);
        std::cout << "Testing LP(LL1, GLR)" << std::endl;
        assert(GLR::parse(table, input, recorder));
        std::cout << recorder << std::endl;
    }
    recorder.clear();
    {
        std::cout << "Building LP(LR1, GLR)" << std::endl;
        GLRTable table = GLR::build(grammar, firsts, disambiguator, true);
        std::cout << "Testing LP(LR1, GLR)" << std::endl;
        assert(GLR::parse(table, input, recorder));
        std::cout << recorder << std::endl;
    }
    recorder.clear();
    {
        std::cout << "Building LP(LL1, LR1, GLR)" << std::endl;
        GLRTable table = GLR::build(grammar, firsts, disambiguator, true, true);
        std::cout << "Testing LP(LL1, LR1, GLR)" << std::endl;
        assert(GLR::parse(table, input, recorder));
        std::cout << recorder << std::endl;
    }
    recorder.clear();

    return 0;
}