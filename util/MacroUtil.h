#pragma once
#ifndef UTIL_INDEX_INCLUDED
#error "Cannot include MacroUtil.h directly. Use Index.h"
#endif
#include "Index.h"

namespace util {
    template<class T>
    inline const T& to_const(T& value); // NOLINT(readability-redundant-declaration)
    template<class T>
    inline T& from_const(const T& value); // NOLINT(readability-redundant-declaration)
    template<class T>
    inline const T* to_const(T* value); // NOLINT(readability-redundant-declaration)
    template<class T>
    inline T* from_const(const T* value); // NOLINT(readability-redundant-declaration)
}

#define UNCONST(FUNC, ...) \
    ::util::from_const(::util::to_const(*this).FUNC(__VA_ARGS__))
