#pragma once
#ifndef UTIL_INDEX_INCLUDED
#error "Cannot include Object.cpp.h directly. Use Index.h"
#endif
#include "Index.h"

namespace util {
    template<class T>
    template<class... Args>
    auto Shared<T>::make(Args&& ... args) -> type {
        return std::make_shared<T>(std::forward<Args>(args)...);
    }

    template<class T>
    template<class... Args>
    auto Unique<T>::make(Args&& ... args) -> type {
        return std::make_unique<T>(std::forward<Args>(args)...);
    }

    template<class T>
    template<class... Args>
    auto Raw<T>::make(Args&& ... args) -> type {
        return new T(std::forward<Args>(args)...);
    }

    template<class T, class Factory>
    auto Object<T, Factory>::ref() -> Body& {
        return UNCONST(ref);
    }
    template<class T, class Factory>
    auto Object<T, Factory>::ref() const -> const Body& {
        return **this;
    }
    template<class T, class Factory>
    auto Object<T, Factory>::operator *() -> Body& {
        return UNCONST(operator*);
    }
    template<class T, class Factory>
    auto Object<T, Factory>::operator *() const -> const Body& {
        assert(body != nullptr);
        return *body;
    }
    template<class T, class Factory>
    auto Object<T, Factory>::operator ->() -> Body* {
        return &**this;
    }
    template<class T, class Factory>
    auto Object<T, Factory>::operator ->() const -> const Body* {
        return &**this;
    }

    template<class T, class Factory>
    Object<T, Factory>::Object(const Object& obj) :
        body(obj.body)
    { }

    template<class T, class Factory>
    Object<T, Factory>::Object(Object&& obj) noexcept :
        body(std::move(obj.body))
    { }

    template<class T, class Factory>
    template<class... Args>
    Object<T, Factory>::Object(BodyTag, Args&&... args) :
        body(Factory::make(std::forward<Args>(args)...))
    { }

    template<class T, class Factory>
    auto Object<T, Factory>::operator =(nullptr_t) noexcept -> Object& {
        body = nullptr;
        return *this;
    }

    template<class T, class Factory>
    auto Object<T, Factory>::operator =(const Object& obj) -> Object& {
        body = obj.body;
        return *this;
    }

    template<class T, class Factory>
    auto Object<T, Factory>::operator =(Object&& obj) noexcept -> Object& {
        body = std::move(obj.body);
        return *this;
    }

    template<class T, class Factory>
    size_t Object<T, Factory>::EqualityHash::operator()(const Object& obj) const {
        return util::get_hash(obj.ref());
    }

    template<class T, class Factory>
    bool Object<T, Factory>::EqualityEquals::operator ()(const Object& a, const Object& b) const {
        return a.ref() == b.ref();
    }

    template<class T, class Factory>
    size_t Object<T, Factory>::IdentityHash::operator()(const Object& obj) const {
        return util::get_hash(obj.body);
    }

    template<class T, class Factory>
    bool Object<T, Factory>::IdentityEquals::operator ()(const Object& a, const Object& b) const {
        return a.body == b.body;
    }

    template<class T, class Factory>
    bool operator ==(const Object<T, Factory>& a, const Object<T, Factory>& b){
        return a.body == b.body;
    }
    template<class T, class Factory>
    bool operator ==(const Object<T, Factory>& a, nullptr_t b){
        return a.body == b;
    }
    template<class T, class Factory>
    bool operator ==(nullptr_t a, const Object<T, Factory>& b){
        return a == b.body;
    }
    template<class T, class Factory>
    bool operator !=(const Object<T, Factory>& a, const Object<T, Factory>& b){
        return !(a == b);
    }
    template<class T, class Factory>
    bool operator !=(const Object<T, Factory>& a, nullptr_t b){
        return !(a == b);
    }
    template<class T, class Factory>
    bool operator !=(nullptr_t a, const Object<T, Factory>& b){
        return !(a == b);
    }
    template<class T, class Factory>
    std::ostream& operator <<(std::ostream& os, const Object<T, Factory>& obj){
        if(obj == nullptr)
            return os << "null";
        else
            return os << *obj.body;
    }
}