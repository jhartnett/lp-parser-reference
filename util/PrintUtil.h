#pragma once
#ifndef UTIL_INDEX_INCLUDED
#error "Cannot include PrintUtil.h directly. Use Index.h"
#endif
#include "Index.h"

namespace std { // NOLINT(cert-dcl58-cpp)
    template<class T>
    ostream& operator <<(ostream& os, const vector<T>& list); // NOLINT(readability-redundant-declaration)

    template<class T>
    ostream& operator <<(ostream& os, const unordered_set<T>& set); // NOLINT(readability-redundant-declaration)

    template<class K, class V>
    ostream& operator <<(ostream& os, const unordered_map<K, V>& map); // NOLINT(readability-redundant-declaration)

    template<class T1, class T2>
    ostream& operator <<(ostream& os, const pair<T1, T2>& pair); // NOLINT(readability-redundant-declaration)

    template<class... Ts>
    ostream& operator <<(ostream& os, const tuple<Ts...>& tuple); // NOLINT(readability-redundant-declaration)
}
