#pragma once
#ifndef UTIL_INDEX_INCLUDED
#error "Cannot include HashUtil.h directly. Use Index.h"
#endif
#include "Index.h"


namespace util {
    template<size_t prime=31>
    inline void collect_hash(size_t& hash); // NOLINT(readability-redundant-declaration)

    template<size_t prime=31, class T, class... Ts>
    inline void collect_hash(size_t& hash, const T& value, const Ts& ... values); // NOLINT(readability-redundant-declaration)

    template<size_t prime=31, class... Args>
    inline size_t get_hash(const Args&... args); // NOLINT(readability-redundant-declaration)
}

template<class T>
struct std::hash<std::vector<T>> {
    size_t operator ()(const std::vector<T>& vector) const;
};

template<class T, size_t Len>
struct std::hash<std::array<T, Len>> {
    size_t operator ()(const std::array<T, Len>& array) const;
};

template<class T>
struct std::hash<std::unordered_set<T>> {
    size_t operator ()(const std::unordered_set<T>& set) const;
};

template<class K, class V>
struct std::hash<std::unordered_map<K, V>> {
    size_t operator ()(const std::unordered_map<K, V>& set) const;
};

template<class T1, class T2>
struct std::hash<std::pair<T1, T2>> {
    size_t operator ()(const std::pair<T1, T2>& pair) const;
};

template<class... Ts>
struct std::hash<std::tuple<Ts...>> {
    size_t operator ()(const std::tuple<Ts...>& tuple) const;
};