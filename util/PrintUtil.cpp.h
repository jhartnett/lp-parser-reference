#pragma once
#ifndef UTIL_INDEX_INCLUDED
#error "Cannot include PrintUtil.cpp.h directly. Use Index.h"
#endif
#include "Index.h"

namespace std { // NOLINT(cert-dcl58-cpp)
    template<class T>
    ostream& operator <<(ostream& os, const vector<T>& list){
        os << "[";
        bool first = true;
        for(auto& value : list){
            if(!first)
                os << ", ";
            first = false;
            os << value;
        }
        os << "]";
        return os;
    }

    template<class T>
    ostream& operator <<(ostream& os, const unordered_set<T>& set){
        os << "{";
        bool first = true;
        for(auto& value : set){
            if(!first)
                os << ", ";
            first = false;
            os << value;
        }
        os << "}";
        return os;
    }

    template<class K, class V>
    ostream& operator <<(ostream& os, const unordered_map<K, V>& map){
        os << "[";
        bool first = true;
        for(auto& [key, value] : map){
            if(!first)
                os << ", ";
            first = false;
            os << key << ": " << value;
        }
        os << "]";
        return os;
    }

    template<class T1, class T2>
    ostream& operator <<(ostream& os, const pair<T1, T2>& pair){
        os << "(";
        os << pair.first;
        os << ", ";
        os << pair.second;
        os << ")";
        return os;
    }

    namespace detail {
        template<class... Ts, size_t I=0>
        void printTupleHelper(ostream& os, const tuple<Ts...>& tuple){
            if constexpr(I < sizeof...(Ts)){
                if constexpr(I > 0)
                    os << ", ";
                os << get<I>(tuple);
                printTupleHelper<I + 1>(os, tuple);
            }
        }
    }

    template<class... Ts>
    ostream& operator <<(ostream& os, const tuple<Ts...>& tuple){
        os << "(";
        detail::printTupleHelper(os, tuple);
        os << ")";
        return os;
    }
}