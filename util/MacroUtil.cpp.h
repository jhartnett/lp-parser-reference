#pragma once
#ifndef UTIL_INDEX_INCLUDED
#error "Cannot include MacroUtil.cpp.h directly. Use Index.h"
#endif
#include "Index.h"

namespace util {
    template<class T>
    inline const T& to_const(T& value){
        return const_cast<const T&>(value);
    }
    template<class T>
    inline T& from_const(const T& value){
        return const_cast<T&>(value);
    }
    template<class T>
    inline const T* to_const(T* value){
        return const_cast<const T*>(value);
    }
    template<class T>
    inline T* from_const(const T* value){
        return const_cast<T*>(value);
    }
}