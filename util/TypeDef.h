#pragma once

#include <vector>
#include <unordered_set>
#include <unordered_map>

namespace util::types {
    template<class T, size_t Len>
    using Array = std::array<T, Len>;
    template<class T, class Alloc=std::allocator<T>>
    using List = std::vector<T, Alloc>;
    template<class T, class Hash=std::hash<T>, class Pred=std::equal_to<T>, class Alloc=std::allocator<T>>
    using Set = std::unordered_set<T, Hash, Pred, Alloc>;
    template<class K, class V, class Hash=std::hash<K>, class Pred=std::equal_to<K>, class Alloc=std::allocator<std::pair<const K, V>>>
    using Map = std::unordered_map<K, V, Hash, Pred, Alloc>;
}