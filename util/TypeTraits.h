#pragma once
#ifndef UTIL_INDEX_INCLUDED
#error "Cannot include TypeTraits.h directly. Use Index.h"
#endif
#include "Index.h"

namespace std { // NOLINT(cert-dcl58-cpp)
    template<class Iterator>
    decltype(auto) begin(const std::pair<Iterator, Iterator>& pair); // NOLINT(readability-redundant-declaration)
    template<class Iterator>
    decltype(auto) end(const std::pair<Iterator, Iterator>& pair); // NOLINT(readability-redundant-declaration)

    template<class Iterator>
    decltype(auto) begin(const std::tuple<Iterator, Iterator>& pair); // NOLINT(readability-redundant-declaration)
    template<class Iterator>
    decltype(auto) end(const std::tuple<Iterator, Iterator>& pair); // NOLINT(readability-redundant-declaration)
}