#pragma once
#ifndef UTIL_INDEX_INCLUDED
#error "Cannot include HashUtil.cpp.h directly. Use Index.h"
#endif
#include "Index.h"

namespace util {
    template<size_t prime>
    inline void collect_hash(size_t& hash){ }

    template<size_t prime, class T, class... Ts>
    inline void collect_hash(size_t& hash, const T& value, const Ts&... values){
        hash = (hash * prime) ^ std::hash<T>()(value);
        collect_hash<prime>(hash, values...);
    }

    template<size_t prime, size_t I=0, class... Ts>
    inline void collect_hash(size_t& hash, const std::tuple<Ts...>& tuple){
        if constexpr(I < sizeof...(Ts)){
            collect_hash<prime>(hash, std::get<I>(tuple));
            collect_hash<prime, I + 1>(hash, tuple);
        }
    }

    template<size_t prime, class... Args>
    inline size_t get_hash(const Args&... args){
        size_t hash = 0;
        collect_hash<prime>(hash, args...);
        return hash;
    }
}

template<class T>
size_t std::hash<std::vector<T>>::operator ()(const std::vector<T>& vector) const {
    size_t hash = 0;
    for(auto& value : vector)
        util::collect_hash<31>(hash, value);
    return hash;
}

template<class T, size_t Len>
size_t std::hash<std::array<T, Len>>::operator ()(const std::array<T, Len>& array) const {
    size_t hash = 0;
    for(auto& value : array)
        util::collect_hash<31>(hash, value);
    return hash;
}

template<class T>
size_t std::hash<std::unordered_set<T>>::operator ()(const std::unordered_set<T>& set) const {
    size_t hash = 0;
    for(auto& value : set)
        util::collect_hash<1>(hash, value);
    return hash;
}

template<class K, class V>
size_t std::hash<std::unordered_map<K, V>>::operator ()(const std::unordered_map<K, V>& map) const {
    size_t hash = 0;
    for(auto& [key, value] : map)
        util::collect_hash<1>(hash, util::get_hash<31>(key, value));
    return hash;
}

template<class T1, class T2>
size_t std::hash<std::pair<T1, T2>>::operator ()(const std::pair<T1, T2>& pair) const {
    size_t hash = 0;
    util::collect_hash<31>(hash, pair.first, pair.second);
    return hash;
}

template<class... Ts>
size_t std::hash<std::tuple<Ts...>>::operator ()(const std::tuple<Ts...>& tuple) const {
    size_t hash = 0;
    util::collect_hash<31>(hash, tuple);
    return hash;
}