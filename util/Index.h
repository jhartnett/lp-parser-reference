#pragma once

#include <cstddef>
#include <memory>
#include <array>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <iostream>

#define UTIL_INDEX_INCLUDED

namespace util {
    template<class T>
    struct Shared;
    template<class T>
    struct Unique;
    template<class T>
    struct Raw;
    template<class T, class Factory>
    class Object;
}

#include "HashUtil.h"
#include "MacroUtil.h"
#include "Object.h"
#include "PrintUtil.h"
#include "TypeDef.h"
#include "TypeTraits.h"

#include "HashUtil.cpp.h"
#include "MacroUtil.cpp.h"
#include "Object.cpp.h"
#include "PrintUtil.cpp.h"
#include "TypeTraits.cpp.h"

#undef UTIL_INDEX_INCLUDED