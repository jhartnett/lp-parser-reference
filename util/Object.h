#pragma once
#ifndef UTIL_INDEX_INCLUDED
#error "Cannot include Object.h directly. Use Index.h"
#endif
#include "Index.h"


namespace util {
    template<class T>
    struct Shared {
        using type = std::shared_ptr<T>;

        template<class... Args>
        inline static type make(Args&&... args);
    };

    template<class T>
    struct Unique {
        using type = std::unique_ptr<T>;

        template<class... Args>
        inline static type make(Args&&... args);
    };

    template<class T>
    struct Raw {
        using type = T*;

        template<class... Args>
        inline static type make(Args&&... args);
    };

    template<class T, class Factory>
    bool operator ==(const Object<T, Factory>& a, const Object<T, Factory>& b);
    template<class T, class Factory>
    bool operator ==(const Object<T, Factory>& a, nullptr_t b);
    template<class T, class Factory>
    bool operator ==(nullptr_t a, const Object<T, Factory>& b);
    template<class T, class Factory>
    bool operator !=(const Object<T, Factory>& a, const Object<T, Factory>& b);
    template<class T, class Factory>
    bool operator !=(const Object<T, Factory>& a, nullptr_t b);
    template<class T, class Factory>
    bool operator !=(nullptr_t a, const Object<T, Factory>& b);
    template<class T, class Factory>
    std::ostream& operator <<(std::ostream& os, const Object<T, Factory>& obj);

    static struct BodyTag { } body_tag;

    template<class T, class Factory=Shared<T>>
    class Object {
    public:
        using Body = T;
        using Ptr = typename Factory::type;
    protected:
        Ptr body;

        Body& ref();
        const Body& ref() const;
        Body& operator *();
        const Body& operator *() const;
        Body* operator ->();
        const Body* operator ->() const;
    public:
        constexpr Object(nullptr_t=nullptr) noexcept : body() { } // NOLINT(google-explicit-constructor)
        Object(const Object& obj);
        Object(Object&& obj) noexcept;
        template<class... Args>
        explicit Object(BodyTag, Args&&... args);

        Object& operator =(nullptr_t) noexcept;
        Object& operator =(const Object& obj);
        Object& operator =(Object&& obj) noexcept;

        friend std::hash<Object>;
        friend bool operator == <T, Factory>(const Object& a, const Object& b);
        friend bool operator == <T, Factory>(const Object& a, nullptr_t b);
        friend bool operator == <T, Factory>(nullptr_t a, const Object& b);
        friend bool operator != <T, Factory>(const Object& a, const Object& b);
        friend bool operator != <T, Factory>(const Object& a, nullptr_t b);
        friend bool operator != <T, Factory>(nullptr_t a, const Object& b);
        friend std::ostream& operator << <T, Factory>(std::ostream& os, const Object& obj);

        struct IdentityHash {
            size_t operator ()(const Object& obj) const;
        };
        struct IdentityEquals {
            bool operator ()(const Object& a, const Object& b) const;
        };
        struct EqualityHash {
            size_t operator ()(const Object& obj) const;
        };
        struct EqualityEquals {
            bool operator ()(const Object& a, const Object& b) const;
        };
    };
}