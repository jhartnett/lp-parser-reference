## LP Parser Reference Implementation

This repository provides a reference implementation for the LP parser.
It includes six LP parser components.

 * LR(1) and GLR super-parsers
 * LL(1) and LR(1) sub-parsers
 * LL(1) and LR(1) prefix-selectors

The components are slotted together into several different configurations and compared to a vanilla GLR parser on a scannerless C-like grammar.
The C-like grammar appears in `/clike/CLike.cpp`. 
On the example C-like program provided in `/example.clike`, the following results are observed.

|     |               |    GLR | LP(LL, GLR) | LP(LR, GLR) | LP(LL, LR, GLR) |
|----:|:--------------|-------:|------------:|------------:|----------------:|
| GLR | Shifts        |  72861 |       50452 |        9791 |            9791 |          
|     | Reduces       | 109802 |       63855 |       13963 |           13963 |
|     | Subparses     |        |         834 |         195 |             195 |
|     | Load Reduction|        |   **37.0%** |   **86.9%** |       **86.9%** |
| LR  | Shifts        |        |             |       17062 |               0 |
|     | Reduces       |        |             |       12263 |            3306 |
|     | Subparses     |        |             |             |            3507 |
|     | Load Reduction|        |             |             |       **76.8%** |
| LL  | Shifts        |        |        4943 |             |            8105 |
|     | Expansions    |        |       10022 |             |           14936 |

For the second and third configurations, the workload for the GLR parser is reduced by almost a full order of magnitude.

### Build/Run Commands

The project requires CMake for building. The following commands are used to compile.

```bash
mkdir build
cd build
cmake ..
cd ..
cmake --build build
```

The binary will appear in `/build/lp_ref_impl`.
It can be run on the example C-like program with:

```bash
./build/lp_ref_impl example.clike
```

Another path can replace `example.clike` to test the parser on a different C-like file.