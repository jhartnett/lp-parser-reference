#include "Index.h"

namespace clike {
    void Disambiguator::operator ()(List<Action>& entries, const Action& action) const {
        if(action.type() == Action::REDUCE){
            const Symbol& next = action.reduce().rule.left();
            if(keywords.count(next)){
                for(auto it = entries.begin(); it != entries.end();){
                    Action& entry = *it;
                    if(entry.type() == Action::REDUCE){
                        const Symbol& prev = entry.reduce().rule.left();
                        if(!keywords.count(prev)){
                            it = entries.erase(it);
                            continue;
                        }
                    }
                    ++it;
                }
            }else{
                for(Action& entry : entries){
                    if(entry.type() == Action::REDUCE){
                        const Symbol& prev = entry.reduce().rule.left();
                        if(keywords.count(prev))
                            return;
                    }
                }
            }
        }
        entries.push_back(action);
    }

    bool Disambiguator::operator ()(Action& entry, const Action& action) const {
        //let keywords have precedence over other symbols
        if(entry.type() == Action::REDUCE && action.type() == Action::REDUCE){
            const Symbol& prev = entry.reduce().rule.left();
            const Symbol& next = action.reduce().rule.left();
            if(keywords.count(prev))
                return !keywords.count(next);
            if(keywords.count(next)){
                entry = action;
                return true;
            }
        }
        return false;
    }

    std::pair<Grammar, Disambiguator> getGrammar(){
        using namespace std::string_literals;

        Grammar grammar { };

        Set<Symbol> keywords { };

        //region Helpers
        size_t tempId = 0;
        Map<std::string, Symbol> literalMap { };
        Map<List<Symbol>, Symbol> optionalMap { };
        Map<List<Symbol>, Symbol> repeatMap { };
        auto temp = [&](){
            return "_"s + std::to_string(tempId++);
        };
        auto literal = [&](const std::string& str, bool keyword=false){
            auto [it, inserted] = literalMap.try_emplace(str, str);
            auto& sym = it->second;
            if(inserted)
                grammar.add({ sym, { str.begin(), str.end() } });
            if(keyword)
                keywords.insert(sym);
            return sym;
        };
        auto optional = [&](List<Symbol> body){
            auto [it, inserted] = optionalMap.try_emplace(body, temp());
            auto& sym = it->second;
            if(inserted){
                grammar.add({ sym, body });
                grammar.add({ sym, { } });
            }
            return sym;
        };
        auto repeat = [&](List<Symbol> body){
            auto [it, inserted] = repeatMap.try_emplace(body, temp());
            auto& sym = it->second;
            if(inserted){
                body.push_back(sym);
                grammar.add({ sym, body });
                grammar.add({ sym, { } });
            }
            return sym;
        };
        //endregion

        Symbol WS("WS");
        Symbol Digit("Digit");
        Symbol NonZeroDigit("NonZeroDigit");
        Symbol HexDigit("HexDigit");
        Symbol OctalDigit("OctalDigit");
        Symbol BinaryDigit("BinaryDigit");

        Symbol _("_");
        Symbol __("__");

        Symbol Program("Program");
        Symbol Statement("Statement");
        Symbol Block("Block");
        Symbol TypeDefinition("TypeDefinition");
        Symbol MemberDefinition("MemberDefinition");
        Symbol VariableDefinition("VariableDefinition");
        Symbol FunctionDefinition("FunctionDefinition");
        Symbol ConstructorDefinition("ConstructorDefinition");
        Symbol Expression("Expression");
        Symbol Expression_("Expression_");
        Symbol Id("Id");
        Symbol Id_("Id_");
        Symbol Variable("Variable");
        Symbol Variable_("Variable_");
        Symbol Destructure("Destructure");
        Symbol DestructureElement("DestructureElement");
        Symbol Argument("Argument");
        Symbol Arguments("Arguments");
        Symbol Modifier_("Modifier_");
        Symbol Type("Type");
        Symbol Type_("Type_");
        Symbol Literal("Literal");
        Symbol NumberLiteral("NumberLiteral");
        Symbol BoolLiteral("BoolLiteral");
        Symbol StringLiteral("StringLiteral");
        Symbol NullLiteral("NullLiteral");

        Symbol InOp("InOp");

        //region Char Classes
        for(auto c = '0'; c <= '9'; c++)
            grammar.add({ Digit, { c }});
        for(auto c = '1'; c <= '9'; c++)
            grammar.add({ NonZeroDigit, { c }});
        for(auto c = '0'; c <= '9'; c++)
            grammar.add({ HexDigit, { c }});
        for(auto c = 'a'; c <= 'f'; c++)
            grammar.add({ HexDigit, { c }});
        for(auto c = 'A'; c <= 'F'; c++)
            grammar.add({ HexDigit, { c }});
        for(auto c = '0'; c <= '7'; c++)
            grammar.add({ OctalDigit, { c }});
        for(auto c = '0'; c <= '1'; c++)
            grammar.add({ BinaryDigit, { c }});
        for(auto c : " \n\t\r")
            grammar.add({ WS, { c }});
        //endregion

        //region Whitespace
        grammar.add({ _, { WS, _ }});
        grammar.add({ _, { }});
        grammar.add({ __, { WS, _ }});
        //endregion

        //region Id
        {
            Symbol IdBody("IdBody");
            Symbol IdTail("IdTail");

            grammar.add({ Id, { IdBody, _ }});
            grammar.add({ Id_, { IdBody, __ }});

            static const std::string firstChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_"s;
            static const std::string nonFirstChars = firstChars + "0123456789"s;

            for(auto c : firstChars)
                grammar.add({ IdBody, { c, IdTail }});
            for(auto c : nonFirstChars)
                grammar.add({ IdTail, { c, IdTail }});
            grammar.add({ IdTail, { }});
        }
        //endregion

        //region Modifiers
        {
            Symbol ModifierBody("ModifierBody");

            grammar.add({ Modifier_, { ModifierBody, __ }});

            grammar.add({ ModifierBody, { literal("public", true) }});
            grammar.add({ ModifierBody, { literal("protected", true) }});
            grammar.add({ ModifierBody, { literal("internal", true) }});
            grammar.add({ ModifierBody, { literal("private", true) }});
            grammar.add({ ModifierBody, { literal("static", true) }});
            grammar.add({ ModifierBody, { literal("abstract", true) }});
            grammar.add({ ModifierBody, { literal("const", true) }});
            grammar.add({ ModifierBody, { literal("getter", true) }});
            grammar.add({ ModifierBody, { literal("setter", true) }});
            grammar.add({ ModifierBody, { literal("operator", true) }});
        }
        //endregion

        //region Type
        {
            Symbol TypeSuffix("TypeSuffix");

            grammar.add({ Type, { Id, repeat({ TypeSuffix }) }});
            grammar.add({ Type_, { Id_ }});
            grammar.add({ Type_, { Id, TypeSuffix, repeat({ TypeSuffix }) }});

            grammar.add({ TypeSuffix, { '<', _, Type, repeat({ ',', _, Type }), '>', _ }});
            grammar.add({ TypeSuffix, { '*', _ }});
            grammar.add({ TypeSuffix, { '[', _, repeat({ ',', _ }), ']', _ }});
        }
        //endregion

        //region Program
        {
            grammar.add({ Program, {
                _, repeat({ TypeDefinition })
            }});
        }
        //endregion

        //region Type Definition
        {
            Symbol IdAndExtends("IdAndExtends");

            grammar.add({ TypeDefinition, { Modifier_, TypeDefinition }});
            grammar.add({ TypeDefinition, {
                literal("class", true), __, IdAndExtends,
                '{', _,
                    repeat({ MemberDefinition }),
                '}', _
            }});

            grammar.add({ IdAndExtends, {
                Id
            }});
            grammar.add({ IdAndExtends, {
                Id_, literal("extends", true), __, Type, repeat({ ',', _, Type })
            }});
        }
        //endregion

        //region Arguments
        {
            grammar.add({ Variable, { Type_, Id }});
            grammar.add({ Variable, { Type, Destructure }});
            grammar.add({ Variable_, { Type_, Id_ }});
            grammar.add({ Variable_, { Type, Destructure }});

            grammar.add({ Destructure, { '[', _, DestructureElement, repeat({ ',', _, DestructureElement }), ']', _ }});
            grammar.add({ DestructureElement, { Id }});
            grammar.add({ DestructureElement, { Destructure }});

            grammar.add({ Argument, { Variable, optional({ '=', _, Expression }) }});

            grammar.add({ Arguments, {
                Argument, repeat({ ',', _, Argument })
            }});
        }
        //endregion

        //region Member Definitions
        {
            grammar.add({ MemberDefinition, { VariableDefinition } });
            grammar.add({ MemberDefinition, { FunctionDefinition } });
            grammar.add({ MemberDefinition, { ConstructorDefinition } });

            grammar.add({ VariableDefinition, { Modifier_, VariableDefinition }});
            grammar.add({ VariableDefinition, {
                Type_, Id, optional({ '=', _, Expression }), ';', _
            }});

            grammar.add({ FunctionDefinition, { Modifier_, FunctionDefinition }});
            grammar.add({ FunctionDefinition, {
                Type_, Id, '(', _, optional({ Arguments }), ')', _,
                    Block
            }});

            grammar.add({ ConstructorDefinition, { Modifier_, ConstructorDefinition }});
            grammar.add({ ConstructorDefinition, {
                Id, '(', _, optional({ Arguments }), ')', _,
                    Block
            }});
        }
        //endregion

        //region Literals
        {
            Symbol LiteralBody("LiteralBody");

            grammar.add({ Literal, { LiteralBody, _ }});

            grammar.add({ LiteralBody, { NumberLiteral }});
            grammar.add({ LiteralBody, { BoolLiteral }});
            grammar.add({ LiteralBody, { StringLiteral }});
            grammar.add({ LiteralBody, { NullLiteral }});

            //region Number Literals
            {
                //region Decimal
                {
                    Symbol Digit0("Digit0");
                    Symbol Digit1("Digit1");
                    
                    grammar.add({ Digit0, { Digit, Digit0 }});
                    grammar.add({ Digit0, { } });
                    grammar.add({ Digit1, { Digit, Digit0 }});

                    grammar.add({ NumberLiteral, {
                        '0', optional({ '.', Digit0 })
                    }});
                    grammar.add({ NumberLiteral, {
                        NonZeroDigit, Digit0, optional({ '.', Digit0 })
                    }});
                    grammar.add({ NumberLiteral, {
                        '.', Digit1
                    }});
                }
                //endregion
                //region Hex
                {
                    Symbol HexDigit0("HexDigit0");
                    Symbol HexDigit1("HexDigit1");
                    Symbol HexCore("HexCore");

                    grammar.add({ HexDigit0, {HexDigit, HexDigit0} });
                    grammar.add({ HexDigit0, { } });
                    grammar.add({ HexDigit1, {HexDigit, HexDigit0} });

                    grammar.add({ NumberLiteral, {
                        literal("0x"), HexCore
                    }});
                    grammar.add({ HexCore, {
                        HexDigit1, optional({ '.', HexDigit0 })
                    }});
                    grammar.add({ HexCore, {
                        '.', HexDigit1
                    }});
                }
                //endregion
                //region Octal
                {
                    Symbol OctalDigit0("OctalDigit0");
                    Symbol OctalDigit1("OctalDigit1");
                    Symbol OctalCore("OctalCore");

                    grammar.add({ OctalDigit0, {OctalDigit, OctalDigit0} });
                    grammar.add({ OctalDigit0, { } });
                    grammar.add({ OctalDigit1, {OctalDigit, OctalDigit0} });

                    grammar.add(Rule(NumberLiteral, {
                        literal("0o"), OctalCore
                    }));
                    grammar.add({ OctalCore, {
                        OctalDigit1, optional({ '.', OctalDigit0 })
                    }});
                    grammar.add({ OctalCore, {
                        '.', OctalDigit1
                    }});
                }
                //endregion
                //region Binary
                {
                    Symbol BinaryDigit0("BinaryDigit0");
                    Symbol BinaryDigit1("BinaryDigit1");
                    Symbol BinaryCore("BinaryCore");

                    grammar.add({ BinaryDigit0, {BinaryDigit, BinaryDigit0} });
                    grammar.add({ BinaryDigit0, { } });
                    grammar.add({ BinaryDigit1, {BinaryDigit, BinaryDigit0} });

                    grammar.add(Rule(NumberLiteral, {
                        literal("0b"), BinaryCore
                    }));
                    grammar.add({ BinaryCore, {
                        BinaryDigit1, optional({ '.', BinaryDigit0 })
                    }});
                    grammar.add({ BinaryCore, {
                        '.', BinaryDigit1
                    }});
                }
                //endregion
            }
            //endregion
            //region Bool Literals
            {
                grammar.add({ BoolLiteral, { literal("true", true) } });
                grammar.add({ BoolLiteral, { literal("false", true) } });
            }
            //endregion
            //region String/Char Literals
            {
                Symbol Any("Any");
                for(size_t i = 0; i < 256; i++)
                    grammar.add({ Any, {(char)i} });

                Symbol Escaped("Escaped");
                grammar.add({ Escaped, { '\\', Any } });

                Symbol Char1("Char1");
                for(size_t i = 0; i < 256; i++){
                    if(i == '\\' || i == '\n' || i == '\'')
                        continue;
                    grammar.add({ Char1, { (char)i }});
                }
                grammar.add({ Char1, { Escaped } });

                Symbol Char2("Char2");
                for(size_t i = 0; i < 256; i++){
                    if(i == '\\' || i == '\n' || i == '"')
                        continue;
                    grammar.add({ Char2, { (char)i }});
                }
                grammar.add({ Char2, { Escaped } });

                grammar.add(Rule(StringLiteral, {
                    '\'', repeat({ Char1 }), '\''
                }));
                grammar.add(Rule(StringLiteral, {
                    '"', repeat({ Char2 }), '"'
                }));
            }
            //endregion
            //region Null Literals
            {
                grammar.add(Rule(NullLiteral, { literal("null", true) }));
            }
            //endregion
        }
        //endregion

        //region Statement
        {
            Symbol IfStatement("IfStatement");
            Symbol WhileStatement("WhileStatement");
            Symbol ForStatement("ForStatement");
            Symbol BreakStatement("BreakStatement");
            Symbol ContinueStatement("ContinueStatement");
            Symbol ReturnStatement("ReturnStatement");
            Symbol ThrowStatement("ThrowStatement");
            Symbol TryStatement("TryStatement");
            Symbol SwitchStatement("SwitchStatement");
            Symbol CaseStatement("CaseStatement");

            grammar.add({ Block, {
                '{', _, repeat({ Statement }), '}', _
            }});

            grammar.add({ Statement, { ';', _ }});
            grammar.add({ Statement, { Expression, ';', _ }});
            grammar.add({ Statement, { VariableDefinition }});
            grammar.add({ Statement, { Block }});
            grammar.add({ Statement, { IfStatement }});
            grammar.add({ Statement, { WhileStatement }});
            grammar.add({ Statement, { ForStatement }});
            grammar.add({ Statement, { BreakStatement }});
            grammar.add({ Statement, { ContinueStatement }});
            grammar.add({ Statement, { ReturnStatement }});
            grammar.add({ Statement, { ThrowStatement }});
            grammar.add({ Statement, { TryStatement }});
            grammar.add({ Statement, { SwitchStatement }});

            grammar.add({ IfStatement, {
                literal("if", true), _, '(', _, Expression, ')', _,
                    Statement,
                    optional({ literal("else", true), _, Statement })
            }});
            grammar.add({ WhileStatement, {
                literal("while", true), _, '(', _, Expression, ')', _,
                    Statement
            }});
            grammar.add({ ForStatement, {
                literal("for", true), _, '(', _, Variable_, literal("in", true), _, Expression, ')', _,
                    Statement
            }});
            grammar.add({ BreakStatement, { literal("break", true), _, ';', _ }});
            grammar.add({ ContinueStatement, { literal("continue", true), _, ';', _ }});

            grammar.add({ ReturnStatement, { literal("return", true), optional({ __, Expression }), ';', _ }});
            grammar.add({ ThrowStatement, { literal("throw", true), __, Expression, ';', _ }});

            grammar.add({ TryStatement, {
                literal("try", true), _,
                    Statement,
                repeat({
                    literal("catch", true), _, '(', _, Variable, ')', _, Statement
                }),
                optional({
                    literal("finally", true), _, Statement
                })
            }});

            grammar.add({ SwitchStatement, {
                literal("switch", true), _, '(', _, Expression, ')', _,
                    '{', _,
                        repeat({ CaseStatement }),
                    '}', _
            }});

            grammar.add({ CaseStatement, {
                literal("case", true), _, Expression, ':', _, Statement
            }});
            grammar.add({ CaseStatement, {
                literal("default", true), _, ':', _, Statement
            }});
        }
        //endregion

        //region Expression
        {
            Symbol AssignExpression("AssignExpression");
            Symbol AssignExpression_("AssignExpression_");
            Symbol CoalesceExpression("CoalesceExpression");
            Symbol CoalesceExpression_("CoalesceExpression_");
            Symbol IsExpression("IsExpression");
            Symbol IsExpression_("IsExpression_");
            Symbol InExpression("InExpression");
            Symbol InExpression_("InExpression_");
            Symbol OrExpression("OrExpression");
            Symbol OrExpression_("OrExpression_");
            Symbol AndExpression("AndExpression");
            Symbol AndExpression_("AndExpression_");
            Symbol LogicExpression("LogicExpression");
            Symbol LogicExpression_("LogicExpression_");
            Symbol EqualityExpression("EqualityExpression");
            Symbol EqualityExpression_("EqualityExpression_");
            Symbol CompareExpression("CompareExpression");
            Symbol CompareExpression_("CompareExpression_");
            Symbol RangeExpression("RangeExpression");
            Symbol RangeExpression_("RangeExpression_");
            Symbol ShiftExpression("ShiftExpression");
            Symbol ShiftExpression_("ShiftExpression_");
            Symbol AddExpression("AddExpression");
            Symbol AddExpression_("AddExpression_");
            Symbol MultiplyExpression("MultiplyExpression");
            Symbol MultiplyExpression_("MultiplyExpression_");
            Symbol PowerExpression("PowerExpression");
            Symbol PowerExpression_("PowerExpression_");
            Symbol PrefixExpression("PrefixExpression");
            Symbol PrefixExpression_("PrefixExpression_");
            Symbol PostfixExpression("PostfixExpression");
            Symbol PostfixExpression_("PostfixExpression_");
            Symbol SimpleExpression("SimpleExpression");
            Symbol SimpleExpression_("SimpleExpression_");

            Symbol AssignOp("AssignOp");
            Symbol CoalesceOp("CoalesceOp");
            Symbol IsOp("IsOp");
            Symbol OrOp("OrOp");
            Symbol AndOp("AndOp");
            Symbol LogicOp("LogicOp");
            Symbol EqualityOp("EqualityOp");
            Symbol CompareOp("CompareOp");
            Symbol RangeOp("RangeOp");
            Symbol ShiftOp("ShiftOp");
            Symbol AddOp("AddOp");
            Symbol MultiplyOp("MultiplyOp");
            Symbol PowerOp("PowerOp");
            Symbol PrefixOp("PrefixOp");
            Symbol PostfixOp("PostfixOp");
            Symbol PostfixOp_("PostfixOp_");
            Symbol Entry("Entry");

            Symbol ExpressionList("ExpressionList");

            grammar.add({ Expression, { AssignExpression }});
            grammar.add({ Expression_, { AssignExpression_ }});

            //region AssignExpression
            grammar.add({ AssignExpression, { CoalesceExpression }});
            grammar.add({ AssignExpression, { CoalesceExpression, AssignOp, _, AssignExpression }});
            grammar.add({ AssignExpression_, { CoalesceExpression_ }});
            grammar.add({ AssignExpression_, { CoalesceExpression, AssignOp, _, AssignExpression_ }});
            grammar.add({ AssignOp, { literal("=") }});
            grammar.add({ AssignOp, { CoalesceOp, literal("=") }});
            grammar.add({ AssignOp, { OrOp, literal("=") }});
            grammar.add({ AssignOp, { AndOp, literal("=") }});
            grammar.add({ AssignOp, { LogicOp, literal("=") }});
            grammar.add({ AssignOp, { ShiftOp, literal("=") }});
            grammar.add({ AssignOp, { AddOp, literal("=") }});
            grammar.add({ AssignOp, { MultiplyOp, literal("=") }});
            grammar.add({ AssignOp, { PowerOp, literal("=") }});
            //endregion
            //region CoalesceExpression
            grammar.add({ CoalesceExpression, { IsExpression }});
            grammar.add({ CoalesceExpression, { IsExpression, CoalesceOp, _, CoalesceExpression }});
            grammar.add({ CoalesceExpression_, { IsExpression_ }});
            grammar.add({ CoalesceExpression_, { IsExpression, CoalesceOp, _, CoalesceExpression_ }});
            grammar.add({ CoalesceOp, { literal("?\?") }});
            //endregion
            //region IsExpression
            grammar.add({ IsExpression, { InExpression }});
            grammar.add({ IsExpression, { InExpression_, IsOp, _, IsExpression }});
            grammar.add({ IsExpression_, { InExpression_ }});
            grammar.add({ IsExpression_, { InExpression_, IsOp, _, IsExpression_ }});
            grammar.add({ IsOp, { optional({ '!', _ }), literal("is", true) }});
            //endregion
            //region InExpression
            grammar.add({ InExpression, { OrExpression }});
            grammar.add({ InExpression, { OrExpression_, InOp, _, InExpression }});
            grammar.add({ InExpression_, { OrExpression_ }});
            grammar.add({ InExpression_, { OrExpression_, InOp, _, InExpression_ }});
            grammar.add({ InOp, { optional({ '!', _ }), literal("in", true) }});
            //endregion
            //region OrExpression
            grammar.add({ OrExpression, { AndExpression }});
            grammar.add({ OrExpression, { AndExpression, OrOp, _, OrExpression }});
            grammar.add({ OrExpression_, { AndExpression_ }});
            grammar.add({ OrExpression_, { AndExpression, OrOp, _, OrExpression_ }});
            grammar.add({ OrOp, { literal("||") }});
            //endregion
            //region AndExpression
            grammar.add({ AndExpression, { LogicExpression }});
            grammar.add({ AndExpression, { LogicExpression, AndOp, _, AndExpression }});
            grammar.add({ AndExpression_, { LogicExpression_ }});
            grammar.add({ AndExpression_, { LogicExpression, AndOp, _, AndExpression_ }});
            grammar.add({ AndOp, { literal("&&") }});
            //endregion
            //region LogicExpression
            grammar.add({ LogicExpression, { EqualityExpression }});
            grammar.add({ LogicExpression, { EqualityExpression, LogicOp, _, LogicExpression }});
            grammar.add({ LogicExpression_, { EqualityExpression_ }});
            grammar.add({ LogicExpression_, { EqualityExpression, LogicOp, _, LogicExpression_ }});
            grammar.add({ LogicOp, { '|' }});
            grammar.add({ LogicOp, { '&' }});
            //endregion
            //region EqualityExpression
            grammar.add({ EqualityExpression, { CompareExpression }});
            grammar.add({ EqualityExpression, { CompareExpression, EqualityOp, _, EqualityExpression }});
            grammar.add({ EqualityExpression_, { CompareExpression_ }});
            grammar.add({ EqualityExpression_, { CompareExpression, EqualityOp, _, EqualityExpression_ }});
            grammar.add({ EqualityOp, { literal("==") }});
            grammar.add({ EqualityOp, { literal("!=") }});
            //endregion
            //region CompareExpression
            grammar.add({ CompareExpression, { RangeExpression }});
            grammar.add({ CompareExpression, { RangeExpression, CompareOp, _, CompareExpression }});
            grammar.add({ CompareExpression_, { RangeExpression_ }});
            grammar.add({ CompareExpression_, { RangeExpression, CompareOp, _, CompareExpression_ }});
            grammar.add({ CompareOp, { literal("<") }});
            grammar.add({ CompareOp, { literal(">") }});
            grammar.add({ CompareOp, { literal("<=") }});
            grammar.add({ CompareOp, { literal(">=") }});
            //endregion
            //region RangeExpression
            grammar.add({ RangeExpression, { ShiftExpression }});
            grammar.add({ RangeExpression, { ShiftExpression, RangeOp, _, optional({ RangeExpression }) }});
            grammar.add({ RangeExpression, { RangeOp, _, RangeExpression }});
            grammar.add({ RangeExpression_, { ShiftExpression_ }});
            grammar.add({ RangeExpression_, { ShiftExpression, RangeOp, _, optional({ RangeExpression_ }) }});
            grammar.add({ RangeExpression_, { RangeOp, _, RangeExpression_ }});
            grammar.add({ RangeOp, { literal(":") }});
            //endregion
            //region ShiftExpression
            grammar.add({ ShiftExpression, { AddExpression }});
            grammar.add({ ShiftExpression, { AddExpression, ShiftOp, _, ShiftExpression }});
            grammar.add({ ShiftExpression_, { AddExpression_ }});
            grammar.add({ ShiftExpression_, { AddExpression, ShiftOp, _, ShiftExpression_ }});
            grammar.add({ ShiftOp, { literal("<<") }});
            grammar.add({ ShiftOp, { literal(">>") }});
            //endregion
            //region AddExpression
            grammar.add({ AddExpression, { MultiplyExpression }});
            grammar.add({ AddExpression, { MultiplyExpression, AddOp, _, AddExpression }});
            grammar.add({ AddExpression_, { MultiplyExpression_ }});
            grammar.add({ AddExpression_, { MultiplyExpression, AddOp, _, AddExpression_ }});
            grammar.add({ AddOp, { literal("+") }});
            grammar.add({ AddOp, { literal("-") }});
            //endregion
            //region MultiplyExpression
            grammar.add({ MultiplyExpression, { PowerExpression }});
            grammar.add({ MultiplyExpression, { PowerExpression, MultiplyOp, _, MultiplyExpression }});
            grammar.add({ MultiplyExpression_, { PowerExpression_ }});
            grammar.add({ MultiplyExpression_, { PowerExpression, MultiplyOp, _, MultiplyExpression_ }});
            grammar.add({ MultiplyOp, { literal("*") }});
            grammar.add({ MultiplyOp, { literal("/") }});
            grammar.add({ MultiplyOp, { literal("%") }});
            //endregion
            //region PowerExpression
            grammar.add({ PowerExpression, { PrefixExpression }});
            grammar.add({ PowerExpression, { PowerExpression, PowerOp, _, PrefixExpression }});
            grammar.add({ PowerExpression_, { PrefixExpression_ }});
            grammar.add({ PowerExpression_, { PowerExpression, PowerOp, _, PrefixExpression_ }});
            grammar.add({ PowerOp, { literal("**") }});
            //endregion
            //region PrefixExpression
            grammar.add({ PrefixExpression, { PostfixExpression }});
            grammar.add({ PrefixExpression, { PrefixOp, _, PrefixExpression }});
            grammar.add({ PrefixExpression_, { PostfixExpression_ }});
            grammar.add({ PrefixExpression_, { PrefixOp, _, PrefixExpression_ }});
            grammar.add({ PrefixOp, { '+' }});
            grammar.add({ PrefixOp, { '-' }});
            grammar.add({ PrefixOp, { literal("++") }});
            grammar.add({ PrefixOp, { literal("--") }});
            grammar.add({ PrefixOp, { '~' }});
            grammar.add({ PrefixOp, { '!' }});
            grammar.add({ PrefixOp, { '*' }});
            grammar.add({ PrefixOp, { '&' }});
            //endregion
            //region PostfixExpression
            grammar.add({ PostfixExpression, { SimpleExpression }});
            grammar.add({ PostfixExpression, { PostfixExpression, PostfixOp }});
            grammar.add({ PostfixExpression_, { SimpleExpression_ }});
            grammar.add({ PostfixExpression_, { PostfixExpression, PostfixOp_ }});
            grammar.add({ PostfixOp, { literal("++"), _ }});
            grammar.add({ PostfixOp, { literal("--"), _ }});
            grammar.add({ PostfixOp, { '.', _, Id }});
            grammar.add({ PostfixOp, { '[', _, optional({ ExpressionList }), ']', _ }});
            grammar.add({ PostfixOp, { '(', _, optional({ ExpressionList }), ')', _ }});
            grammar.add({ PostfixOp_, { literal("++"), _ }});
            grammar.add({ PostfixOp_, { literal("--"), _ }});
            grammar.add({ PostfixOp_, { '.', _, Id_ }});
            grammar.add({ PostfixOp_, { '[', _, optional({ ExpressionList }), ']', _ }});
            grammar.add({ PostfixOp_, { '(', _, optional({ ExpressionList }), ')', _ }});
            //endregion
            //region SimpleExpression
            grammar.add({ SimpleExpression, { Literal }});
            grammar.add({ SimpleExpression, { Id }});
            grammar.add({ SimpleExpression, { literal("new", true), _, Type, '(', _, optional({ ExpressionList }), ')', _ }});
            grammar.add({ SimpleExpression, { '(', _, optional({ ExpressionList }), ')', _ }});
            grammar.add({ SimpleExpression, { '[', _, optional({ ExpressionList }), ']', _ }});
            grammar.add({ SimpleExpression, { '{', _, optional({ ExpressionList }), '}', _ }});
            grammar.add({ SimpleExpression_, { Literal }});
            grammar.add({ SimpleExpression_, { Id_ }});
            grammar.add({ SimpleExpression_, { literal("new", true), _, Type, '(', _, optional({ ExpressionList }), ')', _ }});
            grammar.add({ SimpleExpression_, { '(', _, optional({ ExpressionList }), ')', _ }});
            grammar.add({ SimpleExpression_, { '[', _, optional({ ExpressionList }), ']', _ }});
            grammar.add({ SimpleExpression_, { '{', _, optional({ ExpressionList }), '}', _ }});
            //endregion

            grammar.add({ ExpressionList, { Expression, repeat({ ',', _, Expression }) }});
        }
        //endregion

        grammar.add({ Symbol::START, { Program } });

        return {grammar, { std::move(keywords) }};
    }
}