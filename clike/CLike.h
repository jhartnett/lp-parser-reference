#pragma once
#ifndef CLIKE_INDEX_INCLUDED
#error "Cannot include CLike.h directly. Use Index.h"
#endif
#include "Index.h"

namespace clike {
    struct Disambiguator {
        Set<Symbol> keywords;

        void operator ()(List<Action>& entries, const Action& action) const;
        bool operator ()(Action& entry, const Action& action) const;
    };

    std::pair<Grammar, Disambiguator> getGrammar();
}