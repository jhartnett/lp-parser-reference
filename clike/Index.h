#pragma once

#include "../grammar/Index.h"
#include "../parser/Index.h"

#define CLIKE_INDEX_INCLUDED

namespace clike {
    using namespace grammar;
    using namespace parser;
}

#include "CLike.h"

#undef CLIKE_INDEX_INCLUDED