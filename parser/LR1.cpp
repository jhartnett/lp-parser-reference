#include <sstream>
#include "Index.h"


namespace parser {
    std::string LR1::NAME = "LR1"; // NOLINT(cert-err58-cpp)

    void LR1::Shifter::handle(const Symbol& A) const {
        auto& shift = getShift(table, stack.back(), A);
        handle(shift.id);
    }

    void LR1::Shifter::handle(size_t id) const {
        stack.push_back(id);
    }

    LR1::Shifter::Shifter(const LR1::Table& table, List<size_t>& stack) :
        table(table),
        stack(stack)
    { }

    auto LR1::getClosure(const Grammar& grammar, const Firsts& firsts, const ItemSet& kernel) -> ItemSet {
        ItemSet closure { kernel };
        bool change;
        do{
            change = false;
            for(auto& [item, lookaheads] : closure){
                auto& B = item.next();
                if(B == Symbol::EPSILON)
                    continue;
                auto beta = std::make_pair(item.rule.begin() + item.i + 1, item.rule.end());
                auto follows = getFirsts(firsts, beta, lookaheads);
                for(auto& rule : grammar.rules(B)){
                    Set<Symbol>& set = closure[rule];
                    for(auto& C : follows)
                        change |= set.insert(C).second;
                }
            }
        }while(change);
        return closure;
    }

    auto LR1::getGotos(const ItemSet& closure) -> Map<Symbol, ItemSet> {
        Map<Symbol, ItemSet> gotos { };
        for(auto& [item, lookaheads] : closure){
            auto& B = item.next();
            if(B == Symbol::EPSILON)
                continue;
            gotos[B][item + 1] = lookaheads;
        }
        return gotos;
    }

    const Shift& LR1::getShift(const Table& table, size_t id, const Symbol& A) {
        auto [ptr, exists] = tryGetShift(table, id, A);
        if(!exists)
            throw std::runtime_error("Invalid LR1 table!");
        return *ptr;
    }

    std::pair<const Shift*, bool> LR1::tryGetShift(const Table& table, size_t id, const Symbol& A) {
        auto it = table.find({id, A});
        if(it != table.end()){
            const Action& action = it->second;
            if(action.type() == Action::SHIFT || action.type() == Action::SUBPARSE_LL1)
                return {&action.shift(), true};
        }
        return {nullptr, false};
    }

    void LR1::prefix(const Grammar& grammar, const Firsts& firsts, const Map<GLRItemSet, std::pair<size_t, GLRItemSet>>& states, GLRTable& table, bool enableLL1) {
        auto subTable = std::make_shared<Table>();

        for(auto& [pair, actions] : table){
            auto& [id, A] = pair;
            if(actions.size() == 1){
                Action& action = actions[0];
                subTable->try_emplace({id, A}, action);
                if(action.type() == Action::SHIFT || (action.type() == Action::REDUCE && action.reduce().rule.empty()))
                    actions.push_back(LR1::Subparse(subTable, id));
            }
        }

        if(enableLL1)
            LL1::prefix(grammar, firsts, states, *subTable);
    }
}