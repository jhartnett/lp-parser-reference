#pragma once
#ifndef PARSER_INDEX_INCLUDED
#error "Cannot include LL1.cpp.h directly. Use Index.h"
#endif
#include "Index.h"
#include <sstream>

namespace parser {
    template<class Disambiguator>
    auto LL1::build(const Grammar& grammar, const Firsts& firsts, const Follows& follows, const Disambiguator& disambiguator) -> Table {
        Table table { };
        for(auto& rule : grammar.rules()){
            auto& [A, alpha] = rule;
            for(auto& B : getFirsts(firsts, alpha, follows.at(A))){
                auto [ptr, inserted] = table.try_emplace({A, B}, rule);
                if(!inserted){
                    auto& entry = ptr->second;
                    bool resolved = false;
                    if constexpr(!std::is_same_v<Disambiguator, nullptr_t>)
                        resolved = disambiguator(entry, rule);
                    if(!resolved){
                        std::stringstream str { };
                        str << "Grammar is not LL(1)!\n";
                        str << "Conflict expanding " << A << " seeing " << B << "\n";
                        str << "  " << entry << "\n";
                        str << "  " << rule << "\n";
                        throw std::runtime_error(str.str());
                    }
                }
            }
        }
        return table;
    }

    template<class Iterable>
    bool LL1::parse(const Table& table, const Iterable& iterable, Recorder& recorder){
        std::function<bool(const Symbol&, const Symbol&)> expand;

        Input input { iterable };
        auto& steps = recorder[NAME];

        List<Item<>> stack{ };

        expand = [&](const Symbol& A, const Symbol& B) -> bool {
            auto it = table.find({A, B});
            if(it != table.end()){
                const Rule& rule = it->second;
                stack.emplace_back(rule);
                return true;
            }else{
                return false;
            }
        };

        steps["expansions"]++;
        if(!expand(Symbol::START, *input))
            return false;

        while(!stack.empty()){
            Item<>& item = stack.back();
            if(item.next() == Symbol::EPSILON){
                stack.pop_back();
                continue;
            }
            ++item;
            const Symbol& B = item.prev();
            const Symbol& C = *input;
            if(B == C){
                steps["shifts"]++;
                ++input;
            }else{
                steps["expansions"]++;
                if(!expand(B, C))
                    return false;
            }
        }
        return input.empty();
    }

    template<class Iterable, class ParentShifter>
    void LL1::subParse(const Subparse& args, Input<Iterable>& input, const ParentShifter& shifter, Recorder& recorder) {
        std::function<bool(const Symbol&, const Symbol&)> expand;
        auto& table = *args.table;
        auto& steps = recorder[NAME];

        List<Item<>> stack{ };

        expand = [&](const Symbol& A, const Symbol& B) -> bool {
            auto it = table.find({A, B});
            if(it != table.end()){
                steps["expansions"]++;
                const Rule& rule = it->second;
                stack.emplace_back(rule);
                return true;
            }else{
                return false;
            }
        };

        if(!expand(args.start, *input))
            return;

        while(!stack.empty()){
            Item<>& item = stack.back();
            if(item.next() == Symbol::EPSILON){
                stack.pop_back();
                continue;
            }
            ++item;
            const Symbol& B = item.prev();
            const Symbol& C = *input;
            if(B == C){
                steps["shifts"]++;
                ++input;
            }else{
                if(!expand(B, C))
                    break;
            }
        }
        size_t shifted = 0;
        for(auto& item : stack){
            assert(item.i > 0);
            auto alpha = std::make_pair(item.rule.begin(), item.rule.begin() + item.i - 1);
            shifter(alpha);
            shifted += item.i - 1;
        }
        assert(shifted > 0);
    }
}