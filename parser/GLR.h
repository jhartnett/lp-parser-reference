#pragma once
#ifndef PARSER_INDEX_INCLUDED
#error "Cannot include GLR.h directly. Use Index.h"
#endif
#include "Index.h"

namespace parser {
    struct GLR {
        friend LL1;
        friend LR1;

        static std::string NAME;

        using ItemSet = GLRItemSet;
        using Table = GLRTable;
        struct Shifter;
    private:
        struct NodeBody;
        struct Node;
        friend std::hash<Node>;

        static ItemSet getClosure(const Grammar& grammar, const Firsts& firsts, const ItemSet& kernel);
        static Map<Symbol, ItemSet> getGotos(const ItemSet& closure);
        static const Shift& getShift(const Table& table, size_t id, const Symbol& A);
        static std::pair<const Shift*, bool> tryGetShift(const Table& table, size_t id, const Symbol& A);
    public:
        template<class Disambiguator=nullptr_t>
        static Table build(const Grammar& grammar, const Firsts& firsts, const Disambiguator& disambiguator=Disambiguator(), bool enableLR1=false, bool enableLL1=false);
        template<class Iterable=std::initializer_list<Symbol>>
        static bool parse(const Table& table, const Iterable& input, Recorder& recorder);
    private:
        static Set<Node> getAncestors(const Node& node, size_t count, const Node& target, bool primed=false);
        static void insertAncestors(const Node& node, size_t count, const Node& target, bool primed, Set<Node>& nodes);
    };
}