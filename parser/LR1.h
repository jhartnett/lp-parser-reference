#pragma once
#ifndef PARSER_INDEX_INCLUDED
#error "Cannot include LR1.h directly. Use Index.h"
#endif
#include "Index.h"

namespace parser {
    struct LR1 {
        friend LL1;
        friend GLR;

        static std::string NAME;

        using ItemSet = LR1ItemSet;
        using Table = LR1Table;
        using Subparse = LR1Subparse;
        struct Shifter;
    private:
        static ItemSet getClosure(const Grammar& grammar, const Firsts& firsts, const ItemSet& kernel);
        static Map<Symbol, ItemSet> getGotos(const ItemSet& closure);
        static const Shift& getShift(const Table& table, size_t id, const Symbol& A);
        static std::pair<const Shift*, bool> tryGetShift(const Table& table, size_t id, const Symbol& A);
    public:
        template<class Disambiguator=nullptr_t>
        static Table build(const Grammar& grammar, const Firsts& firsts, const Disambiguator& disambiguator=Disambiguator(), bool enableLL1=false);
    public:
        static void prefix(const Grammar& grammar, const Firsts& firsts, const Map<GLRItemSet, std::pair<size_t, GLRItemSet>>& states, GLRTable& table, bool enableLL1=false);

        template<class Iterable=std::initializer_list<Symbol>>
        static bool parse(const Table& table, const Iterable& input, Recorder& recorder);
        template<class Iterable=std::initializer_list<Symbol>, class ParentShifter>
        static void subParse(const Subparse& args, Input<Iterable>& input, const ParentShifter& shifter, Recorder& recorder);
    };
}