#pragma once
#ifndef PARSER_INDEX_INCLUDED
#error "Cannot include GLR.cpp.h directly. Use Index.h"
#endif
#include "Index.h"
#include "util/Action.h"

namespace parser {
    template<class Disambiguator>
    auto GLR::build(const Grammar& grammar, const Firsts& firsts, const Disambiguator& disambiguator, bool enableLR1, bool enableLL1) -> Table {
        std::function<size_t(const ItemSet& kernel)> insert;
        std::function<void(size_t id, const Symbol& A, Action action)> addEntry;

        Table table { };
        Map<ItemSet, std::pair<size_t, ItemSet>> states { };

        insert = [&](const ItemSet& kernel) -> size_t {
            auto [it, inserted] = states.try_emplace(kernel, states.size(), ItemSet());
            auto& [id, closure] = it->second;
            if(!inserted)
                return id;
            closure = getClosure(grammar, firsts, kernel);
            for(auto& [item, lookaheads] : closure){
                if(item.next() != Symbol::EPSILON)
                    continue;
                for(auto& B : lookaheads)
                    addEntry(id, B, Reduce{item.rule});
            }
            for(auto& [B, gotoKernel] : getGotos(closure)){
                size_t gotoId = insert(gotoKernel);
                addEntry(id, B, Shift{gotoId});
            }
            return id;
        };
        addEntry = [&](size_t id, const Symbol& A, Action action){
            auto [ptr, inserted] = table.try_emplace({id, A});
            List<Action>& entries = ptr->second;
            if(!inserted){
                if constexpr(std::is_same_v<Disambiguator, nullptr_t>)
                    entries.push_back(std::move(action));
                else
                    disambiguator(entries, action);
            }else{
                entries.push_back(std::move(action));
            }
        };

        ItemSet startKernel { };
        for(auto& rule : grammar.rules(Symbol::START))
            startKernel[rule].insert(Symbol::END);
        insert(startKernel);

        if(enableLR1)
            LR1::prefix(grammar, firsts, states, table, enableLL1);
        else if(enableLL1)
            LL1::prefix(grammar, firsts, states, table);
        return table;
    }

    class GLR::Node : public ::util::Object<GLR::NodeBody> {
    public:
        size_t& id();
        size_t id() const;

        Set<Node>& links();
        const Set<Node>& links() const;

        constexpr Node(nullptr_t=nullptr) : Object() { } // NOLINT(google-explicit-constructor)
        explicit Node(size_t id);
        Node(size_t id, Node node);
    };
}

template<>
struct std::hash<parser::GLR::Node> : public parser::GLR::Node::IdentityHash { };

namespace parser {
    struct GLR::NodeBody {
        size_t id;
        Set<Node> links;

        explicit NodeBody(size_t id);
        NodeBody(size_t id, Node node);
    };

    struct GLR::Shifter {
    private:
        const Table& table;
        Node& node;

        void handle(const Symbol& A) const;
        void handle(size_t id) const;
    public:
        Shifter(const GLRTable& table, Node& node);

        template<class Iterable>
        void operator ()(const Iterable& states) const;
    };

    template<class Iterable>
    bool GLR::parse(const Table& table, const Iterable& iterable, Recorder& recorder){
        std::function<void(const Node&, const Symbol&, const Node&)> reducer;

        Input input { iterable };
        auto& steps = recorder[NAME];

        Map<size_t, Node> parsers { };
        parsers.try_emplace(0, 0);

        while(true){
            const Symbol& A = *input;
            List<Node> actors { };
            for(auto& [_, node] : parsers)
                actors.push_back(node);
            List<std::pair<Node, size_t>> shifts { };
            const LR1::Subparse* subparseLR1 = nullptr;
            const LL1::Subparse* subparseLL1 = nullptr;

            reducer = [&](const Node& node, const Symbol& A, const Node& target){
                auto it = table.find({node.id(), A});
                if(it == table.end())
                    return;
                for(const Action& action : it->second){
                    if(action.type() != Action::REDUCE)
                        continue;
                    auto& [B, alpha] = action.reduce().rule;
                    if(B == Symbol::START)
                        continue;
                    steps["reduces"]++;
                    for(const Node& parent : getAncestors(node, alpha.size(), target)){
                        steps["shifts"]++;
                        auto id = getShift(table, parent.id(), B).id;
                        auto [it2, inserted] = parsers.try_emplace(id, id);
                        Node& child = it2->second;
                        if(inserted)
                            actors.push_back(child);
                        auto inserted2 = child.links().insert(parent).second;
                        if(inserted2){
                            for(auto& [_, recheck] : parsers){
                                for(auto& val : actors){
                                    if(val == recheck)
                                        goto next;
                                }
                                reducer(recheck, A, parent);
                                next:;
                            }
                        }
                    }
                }
            };

            while(!actors.empty()){
                Node node = std::move(actors.back());
                actors.pop_back();

                reducer(node, A, nullptr);

                auto it = table.find({node.id(), A});
                if(it != table.end()) {
                    auto& actions = it->second;
                    for(const Action& action : actions){
                        switch(action.type()){
                            case Action::SHIFT: {
                                auto& id = action.shift().id;
                                shifts.emplace_back(node, id);
                                break;
                            }
                            case Action::REDUCE: {
                                auto& [B, alpha] = action.reduce().rule;
                                if(B != Symbol::START)
                                    continue;
                                steps["reduces"]++;
                                return true;
                            }
                            case Action::SUBPARSE_LR1: {
                                subparseLR1 = &action.subparseLR1();
                                break;
                            }
                            case Action::SUBPARSE_LL1: {
                                subparseLL1 = &action.subparseLL1();
                                break;
                            }
                            default: break;
                        }
                    }
                }
            }

            parsers.clear();
            if(shifts.size() == 1 && (subparseLL1 != nullptr || subparseLR1 != nullptr)){
                auto node = shifts[0].first;
                steps["subparses"]++;
                if(subparseLL1 != nullptr)
                    LL1::subParse(*subparseLL1, input, Shifter(table, node), recorder);
                else
                    LR1::subParse(*subparseLR1, input, Shifter(table, node), recorder);
                parsers[node.id()] = node;
            }else{
                for(auto& [node, id] : shifts){
                    steps["shifts"]++;
                    Node& newNode = parsers.try_emplace(id, id).first->second;
                    newNode.links().insert(node);
                }
                if(parsers.empty())
                    return false;
                ++input;
            }
        };
    }

    template<class Iterable>
    void GLR::Shifter::operator ()(const Iterable& states) const {
        for(auto& state : states)
            handle(state);
    }
}