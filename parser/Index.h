#pragma once

#include <variant>
#include <fstream>
#include "../grammar/Index.h"

#define PARSER_INDEX_INCLUDED

namespace parser {
    using namespace grammar;

    template<class Iterator>
    struct Input;

    struct LL1;
    struct LR1;
    struct GLR;

    struct Shift;
    struct Reduce;
    struct LL1Subparse;
    struct LR1Subparse;
    struct Action;

    class Recorder;

    using LL1Table = Map<std::pair<Symbol, Symbol>, Rule>;
    using LR1Table = Map<std::pair<size_t, Symbol>, Action>;
    using GLRTable = Map<std::pair<size_t, Symbol>, List<Action>>;

    using LR1ItemSet = Map<Item<>, Set<Symbol>>;
    using GLRItemSet = Map<Item<>, Set<Symbol>>;
}

#include "Input.h"
#include "LL1.h"
#include "LR1.h"
#include "GLR.h"
#include "util/Action.h"
#include "util/Recorder.h"

#include "Input.cpp.h"
#include "LL1.cpp.h"
#include "LR1.cpp.h"
#include "GLR.cpp.h"
#include "util/Action.cpp.h"

#undef PARSER_INDEX_INCLUDED
