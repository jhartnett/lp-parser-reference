#include <utility>
#include "Index.h"

namespace parser {
    std::string LL1::NAME = "LL1"; // NOLINT(cert-err58-cpp)

    void LL1::prefix(const Grammar& grammar, const Firsts& firsts, const Map<LR1ItemSet, std::pair<size_t, LR1ItemSet>>& states, LR1Table& table){
        std::function<void(size_t)> expandState;

        Map<size_t, Map<Symbol, Action*>> rows { };
        for(auto& [pair, action] : table){
            auto& [id, A] = pair;
            rows[id][A] = &action;
        }

        auto subTable = subBuild(grammar, firsts);

        Map<size_t, Symbol> starts { };

        for(auto& [kernel, entry] : states){
            auto& [id, closure] = entry;
            starts.try_emplace(id, "START-" + std::to_string(id));
        }

        for(auto& [kernel, entry] : states){
            auto& [id, closure] = entry;
            auto graph = getFirstGraph(grammar, firsts, closure);

            for(auto& [A, actionPtr] : rows[id]){
                auto& action = *actionPtr;
                if(action.type() != Action::SHIFT && !(action.type() == Action::REDUCE && action.reduce().rule.empty()))
                    continue;
                Symbol B = traverseFirstGraph(graph, A);
                auto [ptr, exists] = LR1::tryGetShift(table, id, B);
                if(!exists)
                    continue;
                auto& shift = *ptr;
                auto& start = starts.at(id);
                auto& tail = starts.at(shift.id);
                subTable->try_emplace({start, A},
                    Rule(start, {
                        B,
                        tail
                    })
                );

                auto& entry = table.at({id, A});
                entry = LL1::Subparse(entry, subTable, start);
            }
        }
    }

    void LL1::prefix(const Grammar& grammar, const Firsts& firsts, const Map<GLRItemSet, std::pair<size_t, LR1ItemSet>>& states, GLRTable& table){
        std::function<void(size_t)> expandState;

        Map<size_t, Map<Symbol, List<Action>*>> rows { };
        for(auto& [pair, actions] : table){
            auto& [id, A] = pair;
            rows[id][A] = &actions;
        }

        auto subTable = subBuild(grammar, firsts);

        Map<size_t, Symbol> starts { };
        for(auto& [kernel, entry] : states){
            auto& [id, closure] = entry;
            starts.try_emplace(id, "START-" + std::to_string(id));
        }

        for(auto& [kernel, entry] : states){
            auto& [id, closure] = entry;
            auto graph = getFirstGraph(grammar, firsts, closure);

            for(auto& [A, actions] : rows.at(id)){
                for(auto& action : *actions){
                    if(action.type() == Action::SHIFT)
                        goto done;
                    if(action.type() == Action::REDUCE && action.reduce().rule.empty())
                        goto done;
                }
                continue;
                done:
                Symbol B = traverseFirstGraph(graph, A);
                auto [ptr, exists] = GLR::tryGetShift(table, id, B);
                if(!exists)
                    continue;
                auto& shift = *ptr;
                auto& start = starts.at(id);
                auto& tail = starts.at(shift.id);
                subTable->try_emplace({start, A},
                    Rule(start, {
                        B,
                        tail
                    })
                );

                auto& entries = table[{id, A}];
                entries.emplace_back(LL1::Subparse(subTable, start));
            }
        }
    }

    std::shared_ptr<LL1::Table> LL1::subBuild(const Grammar& grammar, const Firsts& firsts) {
        std::function<Set<Symbol>(const Rule&)> getSimpleFirsts;
        std::function<void(const Symbol&, const Symbol&, Set<Symbol>&)> expandSimpleFirsts;

        auto subTablePtr = std::make_shared<Table>();
        auto& subTable = *subTablePtr;
        auto follows = getFollows(grammar, firsts);

        Firsts directFirsts { };
        for(auto& A : grammar.alphabet())
            directFirsts.try_emplace(A);

        for(auto& [A, alpha] : grammar.rules()){
            auto& set = directFirsts.at(A);
            for(auto& B : alpha){
                set.insert(B);
                if(!firsts.at(B).count(Symbol::EPSILON))
                    break;
            }
        }

        getSimpleFirsts = [&](const Rule& rule) -> Set<Symbol> {
            auto& [A, alpha] = rule;
            Set<Symbol> simpleFirsts { };
            for(auto& B : alpha){
                expandSimpleFirsts(A, B, simpleFirsts);
                if(!firsts.at(B).count(Symbol::EPSILON))
                    return simpleFirsts;
            }
            for(auto& B : follows.at(A))
                simpleFirsts.insert(B);
            return simpleFirsts;
        };
        expandSimpleFirsts = [&](const Symbol& A, const Symbol& B, Set<Symbol>& simpleFirsts) -> void {
            if(B == A)
                return;
            auto inserted = simpleFirsts.insert(B).second;
            if(inserted){
                for(auto& C : directFirsts.at(B))
                    expandSimpleFirsts(A, C, simpleFirsts);
            }
        };

        for(auto& rule : grammar.rules()){
            auto& [A, alpha] = rule;
            for(auto& B : getSimpleFirsts(rule)){
                auto [it, inserted] = subTable.try_emplace({A, B}, rule);
                if(!inserted)
                    it->second = nullptr;
            }
        }

        for(auto it = subTable.begin(); it != subTable.end(); ){
            auto& [A, B] = it->first;
            if(it->second == nullptr)
                it = subTable.erase(it);
            else
                ++it;
        }

        return subTablePtr;
    }

    auto LL1::getFirstGraph(const Grammar& grammar, const Firsts& firsts, const LR1ItemSet& closure) -> Map<Symbol, Symbol> {
        using Iter = List<Symbol>::const_iterator;
        std::function<void(Map<Symbol, Symbol>&, const Symbol&, const Symbol&)> insertLink;

        Map<Symbol, Symbol> strongGraph { };
        Map<Symbol, Symbol> weakGraph { };

        insertLink = [&](Map<Symbol, Symbol>& graph, const Symbol& A, const Symbol& B) -> void {
            auto [it, inserted] = graph.try_emplace(B, A);
            if(inserted)
                return;
            it->second  = nullptr;
        };

        for(auto& [item, follows] : closure){
            auto& [A, alpha, B, beta] = item;
            if(!item.rule.empty()){
                if(B == Symbol::EPSILON)
                    continue;
                auto& C = (item.i != 0 || A == Symbol::START) //this is an efficient check for kernel membership
                    ? nullptr
                    : A;
                insertLink(strongGraph, C, B);
            }else{
                for(auto& B : follows)
                    insertLink(weakGraph, A, B);
            }
        }

        for(auto& [A, B] : weakGraph)
            strongGraph.try_emplace(A, B);

        return strongGraph;
    }

    Symbol LL1::traverseFirstGraph(Map<Symbol, Symbol>& graph, const Symbol& A) {
        auto& B = graph.at(A);
        if(B == nullptr)
            return A;
        B = traverseFirstGraph(graph, B);
        return B;
    }
}