#include "Index.h"
#include "GLR.cpp.h"


namespace parser {
    std::string GLR::NAME = "GLR"; // NOLINT(cert-err58-cpp)

    void GLR::Shifter::handle(const Symbol& A) const {
        auto& shift = getShift(table, node.id(), A);
        handle(shift.id);
    }

    void GLR::Shifter::handle(size_t id) const {
        node = Node(id, node);
    }

    GLR::Shifter::Shifter(const GLR::Table& table, Node& node) :
        table(table),
        node(node)
    { }

    auto GLR::getClosure(const Grammar& grammar, const Firsts& firsts, const ItemSet& kernel) -> ItemSet {
        ItemSet closure { kernel };
        bool change;
        do{
            change = false;
            for(auto& [item, lookaheads] : closure){
                auto& B = item.next();
                if(B == Symbol::EPSILON)
                    continue;
                auto beta = std::make_pair(item.rule.begin() + item.i + 1, item.rule.end());
                auto follows = getFirsts(firsts, beta, lookaheads);
                for(auto& rule : grammar.rules(B)){
                    Set<Symbol>& set = closure[rule];
                    for(auto& C : follows)
                        change |= set.insert(C).second;
                }
            }
        }while(change);
        return closure;
    }

    auto GLR::getGotos(const ItemSet& closure) -> Map<Symbol, ItemSet> {
        Map<Symbol, ItemSet> gotos { };
        for(auto& [item, lookaheads] : closure){
            auto& B = item.next();
            if(B == Symbol::EPSILON)
                continue;
            gotos[B][item + 1] = lookaheads;
        }
        return gotos;
    }

    const Shift& GLR::getShift(const Table& table, size_t id, const Symbol& A){
        auto [ptr, exists] = tryGetShift(table, id, A);
        if(!exists)
            throw std::runtime_error("Invalid GLR table!");
        return *ptr;
    }

    std::pair<const Shift*, bool> GLR::tryGetShift(const Table& table, size_t id, const Symbol& A){
        auto it = table.find({id, A});
        if(it != table.end()){
            for(const Action& action : it->second){
                if(action.type() == Action::SHIFT)
                    return {&action.shift(), true};
            }
        }
        return {nullptr, false};
    }

    size_t& GLR::Node::id() {
        return ref().id;
    }

    size_t GLR::Node::id() const {
        return ref().id;
    }

    Set<GLR::Node>& GLR::Node::links() {
        return UNCONST(links);
    }

    const Set<GLR::Node>& GLR::Node::links() const {
        return ref().links;
    }

    GLR::Node::Node(size_t id) :
        Object(::util::body_tag, id)
    { }

    GLR::Node::Node(size_t id, GLR::Node node) :
        Object(::util::body_tag, id, std::move(node))
    { }

    GLR::NodeBody::NodeBody(size_t id) :
        id(id),
        links()
    { }

    GLR::NodeBody::NodeBody(size_t id, Node node) :
        NodeBody(id)
    {
        links.insert(std::move(node));
    }

    auto GLR::getAncestors(const Node& node, size_t count, const Node& target, bool primed) -> Set<Node> {
        Set<Node> ancestors { };
        insertAncestors(node, count, target, primed, ancestors);
        return ancestors;
    }

    void GLR::insertAncestors(const Node& node, size_t count, const Node& target, bool primed, Set<Node>& nodes){
        primed |= node == target;
        if(count == 0){
            if(primed || (target == nullptr))
                nodes.insert(node);
        }else{
            for(auto& link : node.links())
                insertAncestors(link, count - 1, target, primed, nodes);
        }
    }
}