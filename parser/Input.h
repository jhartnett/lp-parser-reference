#pragma once
#ifndef PARSER_INDEX_INCLUDED
#error "Cannot include Input.h directly. Use Index.h"
#endif
#include "Index.h"

namespace parser {
    template<class Iterable>
    struct Input;

    template<class Iterable>
    std::ostream& operator <<(std::ostream& os, const Input<Iterable>& input);

    template<class Iterable>
    struct Input {
    private:
    public:
        using Iterator = decltype(std::begin(std::declval<Iterable>()));

        size_t i;
        Iterator iter;
        Iterator end;
        List<Symbol> backlog;
    public:
        Input(const Iterable& iterable);

        bool empty() const;
        void push_back(Symbol symbol);

        const Symbol operator *() const;
        Input& operator ++();

        friend std::ostream& operator << <Iterable>(std::ostream& os, const Input& input);
    };
}