#pragma once
#ifndef PARSER_INDEX_INCLUDED
#error "Cannot include LL1.h directly. Use Index.h"
#endif
#include "Index.h"

namespace parser {
    struct LL1 {
        static std::string NAME;

        using Table = LL1Table;
        using Subparse = LL1Subparse;

        template<class Disambiguator=nullptr_t>
        static Table build(const Grammar& grammar, const Firsts& firsts, const Follows& follows, const Disambiguator& disambiguator=Disambiguator());

        static void prefix(const Grammar& grammar, const Firsts& firsts, const Map<LR1ItemSet, std::pair<size_t, LR1ItemSet>>& states, LR1Table& table);
        static void prefix(const Grammar& grammar, const Firsts& firsts, const Map<LR1ItemSet, std::pair<size_t, LR1ItemSet>>& states, GLRTable& table);
    private:
        static std::shared_ptr<Table> subBuild(const Grammar& grammar, const Firsts& firsts);
        static Map<Symbol, Symbol> getFirstGraph(const Grammar& grammar, const Firsts& firsts, const LR1ItemSet& closure);
        static Symbol traverseFirstGraph(Map<Symbol, Symbol>& graph, const Symbol& A);
    public:
        template<class Iterable=std::initializer_list<Symbol>>
        static bool parse(const Table& table, const Iterable& input, Recorder& recorder);
        template<class Iterable=std::initializer_list<Symbol>, class ParentShifter>
        static void subParse(const Subparse& args, Input<Iterable>& input, const ParentShifter& shifter, Recorder& recorder);
    };
}
