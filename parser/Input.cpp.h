#pragma once
#ifndef PARSER_INDEX_INCLUDED
#error "Cannot include Input.cpp.h directly. Use Index.h"
#endif
#include "Index.h"
#include "Input.h"


namespace parser {
    template<class Iterable>
    Input<Iterable>::Input(const Iterable& iterable) :
        iter(std::begin(iterable)),
        end(std::end(iterable)),
        backlog()
    { }

    template<class Iterable>
    bool Input<Iterable>::empty() const {
        return iter == end && backlog.empty();
    }

    template<class Iterable>
    void Input<Iterable>::push_back(Symbol symbol) {
        backlog.push_back(std::move(symbol));
    }

    template<class Iterable>
    const Symbol Input<Iterable>::operator *() const {
        if(backlog.size() > 0)
            return backlog.back();
        else if(iter != end)
            return *iter;
        else
            return Symbol::END;
    }

    template<class Iterable>
    auto Input<Iterable>::operator ++() -> Input& {
        if(backlog.size() > 0)
            backlog.pop_back();
        else if(iter != end){
            iter++;
            i++;
        }
        else
            throw std::runtime_error("Input is empty!");
        return *this;
    }

    template<class Iterable>
    std::ostream& operator <<(std::ostream& os, const Input<Iterable>& input){
        for(auto& symbol : std::make_pair(input.backlog.rbegin(), input.backlog.rend()))
            os << symbol << " ";
        return os << std::string(input.iter, input.end);
    }
}