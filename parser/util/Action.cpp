#include "../Index.h"


namespace parser {
    std::ostream& operator <<(std::ostream& os, const Shift& shift) {
        return os << "SHIFT " << shift.id;
    }

    std::ostream& operator <<(std::ostream& os, const Reduce& reduce) {
        return os << "REDUCE " << reduce.rule;
    }

    LL1Subparse::LL1Subparse() :
        isShift(false),
        Shift{},
        table(nullptr),
        start(nullptr)
    { }

    LL1Subparse::LL1Subparse(std::shared_ptr<LL1Table> table, Symbol start) :
        isShift(false),
        Shift{},
        table(std::move(table)),
        start(std::move(start))
    { }

    LL1Subparse::LL1Subparse(const Action& prev, std::shared_ptr<LL1Table> table, Symbol start) :
        isShift(prev.type() == Action::SHIFT),
        Shift{},
        table(std::move(table)),
        start(std::move(start))
    {
        if(isShift)
            id = prev.shift().id;
    }

    std::ostream& operator <<(std::ostream& os, const LL1Subparse& subparse) {
        return os << "SUBPARSE " << subparse.table;
    }

    LR1Subparse::LR1Subparse() :
        table(nullptr),
        start()
    { }

    LR1Subparse::LR1Subparse(std::shared_ptr<LR1Table> table, size_t start) :
        table(std::move(table)),
        start(start)
    { }

    std::ostream& operator <<(std::ostream& os, const LR1Subparse& subparse) {
        return os << "SUBPARSE";
    }

    Action::Action(Shift shift) :
        variant(shift)
    { }

    Action::Action(Reduce reduce) :
        variant(std::move(reduce))
    { }

    Action::Action(LL1Subparse subparse) :
        variant(std::move(subparse))
    { }

    Action::Action(LR1Subparse subparse) :
        variant(std::move(subparse))
    { }

    Action::Type Action::type() const {
        switch(index()){
            case 0: return SHIFT;
            case 1: return REDUCE;
            case 2: return SUBPARSE_LL1;
            case 3: return SUBPARSE_LR1;
            default: throw std::runtime_error("Unknown action type");
        }
    }

    Shift& Action::shift() {
        return UNCONST(shift);
    }

    const Shift& Action::shift() const {
        switch(type()){
            case Action::SHIFT:
                return std::get<Shift>(*this);
            case Action::SUBPARSE_LL1: {
                auto& subparse = std::get<LL1Subparse>(*this);
                if(subparse.isShift)
                    return subparse;
            }
            default:
                throw std::runtime_error("Action is not a shift");
        }
    }

    Reduce& Action::reduce() {
        return UNCONST(reduce);
    }

    const Reduce& Action::reduce() const {
        switch(type()){
            case Action::REDUCE:
                return std::get<Reduce>(*this);
            default:
                throw std::runtime_error("Action is not a reduce");
        }
    }

    LL1Subparse& Action::subparseLL1() {
        return UNCONST(subparseLL1);
    }

    const LL1Subparse& Action::subparseLL1() const {
        switch(type()){
            case Action::SUBPARSE_LL1:
                return std::get<LL1Subparse>(*this);
            default:
                throw std::runtime_error("Action is not an LL1 subparse");
        }
    }

    LR1Subparse& Action::subparseLR1() {
        return UNCONST(subparseLR1);
    }

    const LR1Subparse& Action::subparseLR1() const {
        switch(type()){
            case Action::SUBPARSE_LR1:
                return std::get<LR1Subparse>(*this);
            default:
                throw std::runtime_error("Action is not an LR1 subparse");
        }
    }

    std::ostream& operator <<(std::ostream& os, Action::Type type){
        switch(type){
            case Action::SHIFT:
                return os << "SHIFT";
            case Action::REDUCE:
                return os << "REDUCE";
            case Action::SUBPARSE_LL1:
                return os << "SUBPARSE_LL1";
            case Action::SUBPARSE_LR1:
                return os << "SUBPARSE_LR1";
            default:
                throw std::runtime_error("Unknown action type");
        }
    }

    std::ostream& operator <<(std::ostream& os, const Action& action) {
        switch(action.type()){
            case Action::SHIFT:
                return os << action.shift();
            case Action::REDUCE:
                return os << action.reduce();
            case Action::SUBPARSE_LL1:
                return os << action.subparseLL1();
            case Action::SUBPARSE_LR1:
                return os << action.subparseLR1();
            default:
                throw std::runtime_error("Unknown action type");
        }
    }
}