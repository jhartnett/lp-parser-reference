#pragma once
#ifndef PARSER_INDEX_INCLUDED
#error "Cannot include Action.h directly. Use Index.h"
#endif
#include "../Index.h"
#include <functional>

namespace parser {
    struct Shift {
        size_t id;

        friend std::ostream& operator <<(std::ostream& os, const Shift& shift);
    };

    struct Reduce {
        Rule rule;

        friend std::ostream& operator <<(std::ostream& os, const Reduce& reduce);
    };

    struct LL1Subparse : public Shift {
        bool isShift;
        std::shared_ptr<LL1Table> table;
        Symbol start;

        LL1Subparse();
        LL1Subparse(std::shared_ptr<LL1Table> table, Symbol start);
        LL1Subparse(const Action& prev, std::shared_ptr<LL1Table> table, Symbol start);

        template<size_t N>
        decltype(auto) get();
        template<size_t N>
        decltype(auto) get() const;

        friend std::ostream& operator <<(std::ostream& os, const LL1Subparse& subparse);
    };

    struct LR1Subparse {
        std::shared_ptr<LR1Table> table;
        size_t start;

        LR1Subparse();
        LR1Subparse(std::shared_ptr<LR1Table> table, size_t start);

        template<size_t N>
        decltype(auto) get();
        template<size_t N>
        decltype(auto) get() const;

        friend std::ostream& operator <<(std::ostream& os, const LR1Subparse& subparse);
    };

    struct Action : private std::variant<Shift, Reduce, LL1Subparse, LR1Subparse> {
    public:
        enum class Type : uint8_t {
            SHIFT,
            REDUCE,
            SUBPARSE_LL1,
            SUBPARSE_LR1
        };
        static constexpr Type SHIFT = Type::SHIFT;
        static constexpr Type REDUCE = Type::REDUCE;
        static constexpr Type SUBPARSE_LL1 = Type::SUBPARSE_LL1;
        static constexpr Type SUBPARSE_LR1 = Type::SUBPARSE_LR1;

        constexpr Action() : variant() { };
        Action(Shift shift); // NOLINT(google-explicit-constructor)
        Action(Reduce reduce); // NOLINT(google-explicit-constructor)
        Action(LL1Subparse subparse); // NOLINT(google-explicit-constructor)
        Action(LR1Subparse subparse); // NOLINT(google-explicit-constructor)

        Type type() const;

        Shift& shift();
        const Shift& shift() const;

        Reduce& reduce();
        const Reduce& reduce() const;

        LL1Subparse& subparseLL1();
        const LL1Subparse& subparseLL1() const;

        LR1Subparse& subparseLR1();
        const LR1Subparse& subparseLR1() const;

        friend std::ostream& operator <<(std::ostream& os, Type type);
        friend std::ostream& operator <<(std::ostream& os, const Action& action);
    };
}