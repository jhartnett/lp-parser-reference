#pragma once
#ifndef PARSER_INDEX_INCLUDED
#error "Cannot include Recorder.h directly. Use Index.h"
#endif
#include "../Index.h"

namespace parser {
    class Recorder {
    private:
        Map<std::string, Map<std::string, size_t>> counts;
    public:
        void clear();
        Map<std::string, size_t>& operator [](const std::string& parser);

        friend std::ostream& operator <<(std::ostream& os, const Recorder& recorder);
    };
}
