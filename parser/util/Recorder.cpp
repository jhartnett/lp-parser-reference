#include "../Index.h"
#include "Recorder.h"


namespace parser {
    void Recorder::clear() {
        counts.clear();
    }

    Map<std::string, size_t>& Recorder::operator [](const std::string& parser) {
        return counts[parser];
    }

    std::ostream& operator <<(std::ostream& os, const Recorder& recorder) {
        for(auto& [parser, counts] : recorder.counts){
            if(counts.empty())
                continue;
            os << parser << std::endl;
            for(auto& [action, count] : counts)
                os << "  " << count << " " << action << std::endl;
        }
        return os;
    }
}
