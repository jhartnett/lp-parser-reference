#pragma once
#ifndef PARSER_INDEX_INCLUDED
#error "Cannot include Action.cpp.h directly. Use Index.h"
#endif
#include "../Index.h"

namespace parser {
    template<size_t N>
    decltype(auto) LL1::Subparse::get() {
        if constexpr(N == 0) return *table;
        else if constexpr(N == 1) return start;
    }
    template<size_t N>
    decltype(auto) LL1::Subparse::get() const {
        if constexpr(N == 0) return *table;
        else if constexpr(N == 1) return start;
    }

    template<size_t N>
    decltype(auto) LR1::Subparse::get() {
        if constexpr(N == 0) return *table;
        else if constexpr(N == 1) return start;
    }
    template<size_t N>
    decltype(auto) LR1::Subparse::get() const {
        if constexpr(N == 0) return *table;
        else if constexpr(N == 1) return start;
    }
}