#pragma once
#ifndef PARSER_INDEX_INCLUDED
#error "Cannot include LR1.cpp.h directly. Use Index.h"
#endif
#include "Index.h"

namespace parser {
    template<class Disambiguator>
    auto LR1::build(const Grammar& grammar, const Firsts& firsts, const Disambiguator& disambiguator, bool enableLL1) -> Table {
        std::function<size_t(const ItemSet& kernel)> insert;
        std::function<void(size_t id, const Symbol& A, Action action, const ItemSet&)> addEntry;

        Table table { };
        Map<ItemSet, std::pair<size_t, ItemSet>> states { };

        insert = [&](const ItemSet& kernel) -> size_t {
            auto [it, inserted] = states.try_emplace(kernel, states.size(), ItemSet());
            auto& [id, closure] = it->second;
            if(!inserted)
                return id;
            closure = getClosure(grammar, firsts, kernel);
            for(auto& [item, lookaheads] : closure){
                if(item.next() != Symbol::EPSILON)
                    continue;
                for(auto& B : lookaheads)
                    addEntry(id, B, Reduce{item.rule}, closure);
            }
            for(auto& [B, gotoKernel] : getGotos(closure)){
                size_t gotoId = insert(gotoKernel);
                addEntry(id, B, Shift{gotoId}, closure);
            }
            return id;
        };
        addEntry = [&](size_t id, const Symbol& A, Action action, const ItemSet& closure){
            auto [ptr, inserted] = table.try_emplace({id, A}, action);
            if(!inserted){
                Action& entry = ptr->second;
                bool resolved = false;
                if constexpr(!std::is_same_v<Disambiguator, nullptr_t>)
                    resolved = disambiguator(entry, action);
                if(!resolved){
                    std::stringstream str { };
                    str << "Grammar is not LR(1)!" << std::endl;
                    str << "Conflict at " << id << " seeing " << A << std::endl;
                    str << "  " << entry << std::endl;
                    str << "  " << action << std::endl;
                    for(auto& [item, _] : closure)
                        str << item << std::endl;
                    throw std::runtime_error(str.str());
                }
            }
        };

        ItemSet startKernel { };
        for(auto& rule : grammar.rules(Symbol::START))
            startKernel[rule].insert(Symbol::END);
        insert(startKernel);

        if(enableLL1)
            LL1::prefix(grammar, firsts, states, table);
        return table;
    }

    struct LR1::Shifter {
    private:
        const Table& table;
        List<size_t>& stack;

        void handle(const Symbol& A) const;
        void handle(size_t id) const;
    public:
        Shifter(const Table& table, List<size_t>& stack);

        template<class Iterable>
        void operator ()(const Iterable& states) const;
    };

    template<class Iterable>
    bool LR1::parse(const Table& table, const Iterable& iterable, Recorder& recorder){
        Input input { iterable };
        auto& steps = recorder[NAME];

        List<size_t> stack { 0 };
        while(true){
            size_t state = stack.back();
            const Symbol& A = *input;

            auto it = table.find({state, A});
            if(it != table.end()){
                const Action& action = it->second;
                switch(action.type()){
                    case Action::SHIFT: {
                        auto& id = action.shift().id;
                        steps["shifts"]++;
                        ++input;
                        stack.push_back(id);
                        break;
                    }
                    case Action::REDUCE: {
                        auto& [B, alpha] = action.reduce().rule;
                        steps["reduces"]++;
                        if(B == Symbol::START)
                            return true;
                        stack.erase(stack.end() - alpha.size(), stack.end());
                        input.push_back(B);
                        break;
                    }
                    case Action::SUBPARSE_LL1: {
                        auto& args = action.subparseLL1();
                        steps["subparses"]++;
                        LL1::subParse(args, input, Shifter(table, stack), recorder);
                        break;
                    }
                    default:
                        return false;
                }
            }else{
                return false;
            }
        }
    }

    template<class Iterable, class ParentShifter>
    void LR1::subParse(const Subparse& args, Input<Iterable>& input, const ParentShifter& shifter, Recorder& recorder){
        auto& table = *args.table;

        auto& steps = recorder[NAME];

        List<size_t> stack { args.start };
        while(true){
            size_t state = stack.back();
            const Symbol& A = *input;

            auto it = table.find({state, A});
            if(it != table.end()){
                const Action& action = it->second;
                switch(action.type()){
                    case Action::SHIFT: {
                        auto& id = action.shift().id;
                        steps["shifts"]++;
                        ++input;
                        stack.push_back(id);
                        break;
                    }
                    case Action::REDUCE: {
                        auto& [B, alpha] = action.reduce().rule;
                        steps["reduces"]++;
                        if(B == Symbol::START)
                            goto done;
                        if(alpha.size() >= stack.size())
                            goto done;
                        stack.erase(stack.end() - alpha.size(), stack.end());
                        input.push_back(B);
                        break;
                    }
                    case Action::SUBPARSE_LL1: {
                        steps["subparses"]++;
                        auto& subparse = action.subparseLL1();
                        LL1::subParse(subparse, input, Shifter(table, stack), recorder);
                        break;
                    }
                    default:
                        goto done;
                }
            }else{
                goto done;
            }
        }
        done:
        auto states = std::make_pair(stack.begin() + 1, stack.end());
        shifter(states);
        assert(stack.size() > 1);
    }

    template<class Iterable>
    void LR1::Shifter::operator ()(const Iterable& states) const {
        for(auto& state : states)
            handle(state);
    }
}